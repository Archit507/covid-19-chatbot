// ========================================== IMPORT NPM MODULES ===============================//
const { LuisRecognizer } = require('botbuilder-ai');
//const { CardFactory } = require('botbuilder-core');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
//const { ActivityHandler, MessageFactory } = require('botbuilder');
const { ActivityHandler, CardFactory, ActionTypes, MessageFactory } = require('botbuilder');
const { Activity, ActivityTypes, Attachment, AttachmentLayoutTypes, CardAction, InputHints, SuggestedActions } = require('botframework-schema');

const request = require('request')
const { language } = require('../index.js')
const quick = require('botbuilder-facebook-quick-replies');
var Card = require('./Cards.js')

//const { StateDialog } = require('../stateDialog.js')
var MiscResponse_English = require('../misc_response_Eng.json')
var MiscResponse_Hindi = require('../misc_response_Hin.json')
var MiscResponse_Telugu = require('../misc_response_Tel.json')
var stateLanguage = require('../statelanguage.json')

var fuzz = require('fuzzball')
var states = require('../states.json')
function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}
var numbers = require('../Telephone.json')

// Function to get State Name

async function getStatelanguauge(name, lang) {
    data = ""
    return new Promise((resolve) => {

        stateLanguage.states.forEach(element => {
            if (element[0] == name) {
                if (lang == "te") {
                    data = element
                }
                else if (lang == "en") {
                    data = element
                } else {
                    data = element
                }
            }

        })
        console.log(name, "6666666666666666666666666666666666666666666666", lang, data)
        resolve(data)

    });
}

async function getPhone(statename) {
    prev = 0
    return new Promise((resolve) => {
        states.states.forEach(element => {
            fuzz_ratio = fuzz.token_set_ratio(statename, element);
            if (fuzz_ratio >= 60) {
                // console.log(fuzz_ratio, element)
                numbers.forEach(ele => {
                    if (ele.name == element && prev <= fuzz_ratio) {
                        prev = fuzz_ratio
                        phone = ele.phone
                        resolve({ data: { "phone": phone, "name": ele.name, "status": true } })
                    }
                })
            }
        });

    });
}
async function getTestCenter(element, language) {
    console.log("element", element)
    return new Promise((resolve) => {
        var data;
        request('https://covid19-api.miraclesoft.com/testCenter/retrieve?stateName=' + element + '&language=' + language,
            function (err, response, body) {
                if (body) {
                    data = JSON.parse(body)
                    console.log(data)
                    resolve(data)
                } else {
                    resolve({ data: false })
                }
            })

    });
}

async function getStateName(statename) {
    return new Promise((resolve) => {
        var prev = 0;
        var data;
        states.states.forEach(element => {
            fuzz_ratio = fuzz.token_set_ratio(statename, element);
            if (fuzz_ratio >= 60 && prev <= fuzz_ratio) {
                prev = fuzz_ratio
                console.log(fuzz_ratio, element)
                data = element
                // data=requestPost(element)
            }
        });
        resolve(data)
    });
}


async function requestPost(element) {
    console.log("element", element)
    return new Promise((resolve) => {
        try {
            var data;
            if (element == "India") {
                request('https://covid19-api.miraclesoft.com/states/data?stateName=Total', function (err, response, body) {
                    data = JSON.parse(body)
                    console.log("india count", data)
                    resolve(data)
                })
            }
            else {
                request('https://covid19-api.miraclesoft.com/states/data?stateName=' + element, function (err, response, body) {
                    data = JSON.parse(body)
                    console.log("active", data.data.active)
                    resolve(data)
                })
            }
        } catch (e) {
            console.log(e)
        }

    });
}



class LuisHelper {
    /**
     * Returns an object with preformatted LUIS results for the bot's dialogs to consume.
     * @param {*} logger
     * @param {TurnContext} context
     * 
     */


    static async executeLuisQuery(logger, context) {

        try {

            const recognizer = new LuisRecognizer({
                applicationId: process.env.LuisAppId,
                endpointKey: process.env.LuisAPIKey,
                endpoint: `https://${process.env.LuisAPIHostName}`
            }, {}, true);

            // const recognizerResult = await recognizer.recognize(context);
            // console.log("+++++++", recognizerResult)
            // var intent = LuisRecognizer.topIntent(recognizerResult);
            // console.log(intent)
            // const dispatchrecognizeresult = recognizerResult.luisResult.topScoringIntent;
            // console.log(dispatchrecognizeresult)
            // const intent = dispatchrecognizeresult.intent;

            // var luis = new LuisHelper()
            // var intent2 = await luis.dispatchToTopIntentAsync(context, intent, recognizerResult);

            // console.log(intent2)

            // return intent2
            const recognizerResult = await recognizer.recognize(context);
            const intent = LuisRecognizer.topIntent(recognizerResult);
            console.log(intent)
            var language_detect = context.activity.language
            if (intent == null) {
                return
            }
            var Details = {}
            // console.log(JSON.parse(context.activity.text),JSON.parse(context.activity.text).type,"-------------------------------")

            // // ========================================= GENERAL DIALOGS   =============================================================//
            if (intent == 'Greetings') {

                logger.log('************************************* GREETING_INTENT *********************************');
                console.log("Greetings", context)
                context.sendActivity({ type: 'typing' });
                if (language_detect == 'te')
                    await context.sendActivity("హలో " + context.activity.from.name + "! నా పేరు ఆధ్యా - భారతదేశానికి COVID-19 అసిస్టెన్స్ చాట్‌బాట్");
                else if (language_detect == 'en')
                    await context.sendActivity("Hello " + context.activity.from.name + "! My name is Aadhya - the COVID-19 Assistance Chatbot for India")
                else if (language_detect == 'hi')
                    await context.sendActivity("नमस्कार " + context.activity.from.name + "! मेरा नाम आध्या है - भारत के लिए COVID-19 सहायता चैटबॉट")
                return Details;

            }
            if (intent == 'Que_Covid') {

                logger.log('************************************* Que_Covid *********************************');
                console.log("Greetings", context)
                context.sendActivity({ type: 'typing' });
                if (language_detect == 'te')
                    await context.sendActivity("ఖచ్చితంగా, COVID-19 గురించి మీరు ఏమి తెలుసు కోవాలనుకుంటున్నారు?");
                else if (language_detect == 'en')
                    await context.sendActivity("Sure, what can I help answer around COVID-19?")
                else if (language_detect == 'hi')
                    await context.sendActivity("ज़रूर, आप COVID-19 के बारे में क्या जानना चाहते हैं?")
                return Details;

            }
            else if (intent == 'Capabilities') {
                logger.log('*************************************  CAPABILITY_INTENT *********************************');
                if (language_detect == 'en') {

                    context.sendActivity({ type: 'typing' });
                    await timeout()

                    // await context.sendActivity("I can help you with the following,");
                    //let capability ="I am capable"
                    // let capability = CardFactory.heroCard('', [], ['Questions about COVID-19', 'Testing Center Locations', 'Government Helplines', 'COVID-19 Cases Count'])
                    // // quick.replies(capability)  
                    // // await context.sendActivity(capability)  
                    // await context.sendActivity({ attachments: [capability] });
                    // var reply = MessageFactory.suggestedActions(['Questions about COVID-19', 'Testing Center Locations', 'Government Helplines', 'COVID-19 Cases Count'], 'What language would you like to speak with me in?');
                    // await context.sendActivity(reply);
                    // await context.sendActivity({ attachments: [CardFactory.adaptiveCard(Card["Adaptivecard", "welcomeCard"])], attachmentLayout: AttachmentLayoutTypes.Carousel })
                    
                    var reply = MessageFactory.suggestedActions(['About COVID-19', 'Testing Centers', 'Government Helplines', "COVID-19 Cases Count"],"I can help you with the following,");
                        await context.sendActivity(reply);
                }

                //********************************Telugu****************************** */

                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()

                    //await context.sendActivity("I can help you with the following");
                    //let capability ="I am capable"
                    //let capability = CardFactory.heroCard('', [], ['Questions about COVID-19', 'Testing Center Locations', 'Government Helplines', 'COVID-19 Cases Count'])
                    // quick.replies(capability)  
                    // await context.sendActivity(capability)  
                    //await context.sendActivity({ attachments: [capability] });
                    var reply = MessageFactory.suggestedActions(['COVID-19 గురించి ప్రశ్నలు', 'పరీక్ష కేంద', 'ప్రభుత్వ హెల్ప్‌లైన్‌లు', 'COVID-19 కేసుల సంఖ్య'], 'కింది వాటితో నేను మీకు సహాయం చేయగలను,');
                    await context.sendActivity(reply);

                }
                //***********************************English******************************* */
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()

                    //await context.sendActivity("I can help you with the following");
                    //let capability ="I am capable"
                    //let capability = CardFactory.heroCard('', [], ['Questions about COVID-19', 'Testing Center Locations', 'Government Helplines', 'COVID-19 Cases Count'])
                    // quick.replies(capability)  
                    // await context.sendActivity(capability)  
                    //await context.sendActivity({ attachments: [capability] });
                    var reply = MessageFactory.suggestedActions(['COVID-19 के बारे में प्रश्न', 'परीक्षण केंद्र स्थान', 'सरकारी हेल्पलाइन', 'COVID-19 मामलों की गिनती'], 'मैं निम्नलिखित के साथ आपकी मदद कर सकता हूं,');
                    await context.sendActivity(reply);

                }
                return Details;
            }
            else if (intent == "thankyou") {
                if (language_detect == 'en') {

                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity("Thank you. Be safe, stay indoors and have a nice day!");

                }
                if (language_detect == 'hi') {

                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity("धन्यवाद। सुरक्षित रहें, घर के अंदर रहें और आपका दिन अच्छा रहे!");

                }
                if (language_detect == 'te') {

                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity("ధన్యవాదాలు. సురక్షితంగా ఉండండి, ఇంటి లోపల ఉండండి మరియు శుభదినము!!");

                }
                return Details;

            }
            else if (intent == 'About_Covid-19') {
                logger.log('************************************* About_Covid-19 *********************************');
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid.sub1);
                }
                else if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid.sub1);
                }
                else if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid.sub1);
                }
                return Details;
            }
            // else if (intent == 'intent_State') {
            //     logger.log('************************************* intent_State *********************************');
            //     var entities = recognizerResult.luisResult.entities;
            //     console.log(entities[0].entity)

            //     context.sendActivity({ type: 'typing' });
            //     await timeout()
            //     // let capability = CardFactory.heroCard(' How can I help you today?', [], ['Questions about COVID-19','Testing Center Locations','Government Helplines','COVID-19 Cases Count'])
            //     // await context.sendActivity({ attachments: [capability]});
            //     await context.sendActivity("641 cases were reported today in " + entities[0].entity + "and we have a total of 1251 cases in the state")

            //     return Details;
            // }

            else if (intent == 'Covid-19_symptoms') {
                logger.log('*************************************  Covid-19_symptoms *********************************');
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.covid_symptoms.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.covid_symptoms.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.covid_symptoms.sub1);
                }
                else if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.covid_symptoms.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.covid_symptoms.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.covid_symptoms.sub1);
                }
                else if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.covid_symptoms.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.covid_symptoms.sub);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.covid_symptoms.sub1);
                }
                return Details;

            }
            else if (intent == 'COVID-19_spread') {
                logger.log('************************************* COVID-19_spread *********************************');
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.covid_spread.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.covid_spread.sub);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.covid_spread.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.covid_spread.sub);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.covid_spread.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.covid_spread.sub);
                }


                return Details;

            }
            else if (intent == 'Covid-19_Protection_Measures') {
                logger.log("************************************* Covid-19_Protection_Measures *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.protect_measure.main);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.protect_measure.main);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.protect_measure.main);
                }

                return Details;

            }
            else if (intent == 'Covid-19_Treatment') {
                logger.log("************************************* Covid-19_Treatment *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid_Treatment.main);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid_Treatment.main);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid_Treatment.main);
                }

                return Details;

            }
            else if (intent == 'severe_illness_Persons') {
                logger.log("************************************* severe_illness_Persons *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.severe_illness_Persons.main);
                }
                else if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.severe_illness_Persons.main);
                }
                else if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.severe_illness_Persons.main);
                }


                return Details;

            }
            else if (intent == 'Contact_for_info') {
                logger.log("************************************* Contact_for_info *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Contact_for_info.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Contact_for_info.sub);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Contact_for_info.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Contact_for_info.sub);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Contact_for_info.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Contact_for_info.sub);
                }

                return Details;

            }
            else if (intent == 'Covid-19_Mask') {
                logger.log("*************************************Covid-19_Mask *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Mask.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Mask.sub);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Mask.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Mask.sub);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Mask.main);
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Mask.sub);
                }

                return Details;

            }
            // else if (intent == 'Covid-19_Testplace') {
            //     logger.log("************************************* ANYTHING_ELSE_INTENT *********************************");
            //     context.sendActivity({ type: 'typing' });
            //     await timeout()
            //     await context.sendActivity(MiscResponse_English.Covid_Testplace);
            //     return Details;
            // }
            // else if (intent == 'getstarted') {
            //     var reply = MessageFactory.suggestedActions(['English', 'తెలుగు', 'हिन्दी'], 'What language would you like to speak with me in?');
            //     await context.sendActivity(reply);
            //     return Details
            // }
            else if (intent == 'Covid-19_Survive') {
                // if(JSON.parse(context.activity.text).type=="legacy_reply_to_message_action"){
                //     var reply = MessageFactory.suggestedActions(['English', 'తెలుగు', 'हिन्दी'], 'What language would you like to speak with me in?');
                //     await context.sendActivity(reply);
                //     // return Details
                // }else{
                logger.log("************************************* Covid-19_Survive *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid_Survive);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid_Survive);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid_Survive);
                }


                // }
                return Details;
            }
            else if (intent == 'Covid-19_Stop_Spread') {
                logger.log("************************************* Covid-19_Survive *********************************");
                if (language_detect == 'en') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_English.Covid_Stop_Spread);
                }
                if (language_detect == 'te') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Telugu.Covid_Stop_Spread);
                }
                if (language_detect == 'hi') {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    await context.sendActivity(MiscResponse_Hindi.Covid_Stop_Spread);
                }

                return Details;
            }
            else if (intent == 'None') {
                if (language_detect == 'en') {
                    await context.sendActivity(MiscResponse_English.none);
                }
                if (language_detect == 'te') {
                    await context.sendActivity(MiscResponse_Telugu.none);
                }
                if (language_detect == 'hi') {
                    await context.sendActivity(MiscResponse_Hindi.none);
                }
                return Details;

            }

            //***************************************************************ASSESMENT******************************************************** */
            else if (intent == 'Case_count') {
                logger.log('************************************* Case_count  *********************************');
                context.sendActivity({ type: 'typing' });
                let a = []
                var entities = recognizerResult.luisResult.entities;
                var total = await requestPost("India")
                if (entities != "") {
                    console.log(entities[0].entity)
                    var Name = entities[0].entity;

                    entities.forEach(entity => {
                        if (entity.type == "builtin.geographyV2.countryRegion" || entity.type == "builtin.geographyV2.state" || entity.type == "builtin.datetimeV2.date" || entity.type == "builtin.geographyV2.city") {
                            a.push(entity.type)
                        }
                        console.log(a)
                    })
                }

                if (a.includes("builtin.geographyV2.countryRegion") && (a.includes("builtin.datetimeV2.date"))) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[1] + " కి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.delta.confirmed + " కి చేరుకుంది");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.delta.confirmed + " confirmed cases reported " + data.data.lastupdatedtime.split(" ")[0] + " in India as of " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में आज " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " तक कुल " + data.data.delta.confirmed + " पुष्ट मामले दर्ज किए गए हैं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }

                } else if (a.includes("builtin.geographyV2.countryRegion")) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                else if (a.includes("builtin.geographyV2.state") && !(a.includes("builtin.datetimeV2.date"))) {
                    console.log("Inside else case cour", Name)
                    var currentData = await getStateName(Name)
                    var data = await requestPost(currentData)
                    var aa = await getStatelanguauge(data.data.state, "en")
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(aa[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                        else if (language_detect == 'en') {

                            await context.sendActivity(data.data.confirmed + " cases were reported in " + aa[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (language_detect == 'hi')
                            await context.sendActivity(data.data.confirmed + " मामले " + aa[2] + " में दर्ज किए गए हैं और भारत में कुल " + total.data.confirmed + " मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }



                }
                else if ((a.includes("builtin.datetimeV2.date")) && (a.includes("builtin.geographyV2.state"))) {
                    console.log(entities)
                    var name, date;
                    entities.forEach(d => {
                        if (d.type == 'builtin.datetimeV2.date')
                            date = d.entity
                        if (d.type == 'builtin.geographyV2.state')
                            name = d.entity
                    })

                    var currentData = await getStateName(name)
                    console.log(currentData)
                    var data = await requestPost(currentData)
                    var aa = await getStatelanguauge(data.data.state, "en")
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(aa[1] + " లో " + data.data.lastupdatedtime.split(" ")[0] + " IST నాటికి  " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                        if (language_detect == 'en') {
                            await context.sendActivity(data.data.confirmed + " cases were reported " + data.data.lastupdatedtime.split(" ")[0] + " IST in " + aa[0] + " and we have a total of " + total.data.confirmed + " cases in India")
                        }
                        else if (language_detect == 'hi')
                            await context.sendActivity("आज " + aa[2] + " में " + data.data.confirmed + " मामले दर्ज किए गए थे। भारत में हमारे कुल " + total.data.confirmed + " मामले हैं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                else if ((a.includes("builtin.datetimeV2.date")) && !(a.includes("State"))) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }

                else if (a.includes("builtin.geographyV2.city")) {
                    if (language_detect == 'en') {
                        await context.sendActivity("I’m sorry, that is not a state in India")
                    }
                    if (language_detect == 'te') {
                        await context.sendActivity("క్షమించండి, అది భారతదేశంలో ఉన్న రాష్ట్రం కాదు")
                    }
                    if (language_detect == 'hi') {
                        await context.sendActivity("मुझे खेद है, यह भारत का राज्य नहीं है")
                    }
                    context.sendActivity({ type: 'typing' });
                    await timeout()

                    Details.intent = intent
                    console.log("Details", Details)
                    return Details;
                }
                else // if ((a.includes("builtin.datetimeV2.date") && (a.includes("State")))) {
                {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                            logger.log('*************************************  Date with only state *********************************');
                            return Details;
                        }

                        else if (language_detect == 'en') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                            logger.log('*************************************  STATE_INTENT *********************************');
                            return Details;
                        }
                        else if (language_detect == 'hi') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                            logger.log('*************************************  STATE_INTENT *********************************');
                            return Details;
                        }
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                return Details;
                // else {
                //     var data = await requestPost("India")
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1])
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }
                // }


            }

            //*************************************************RECOVERY CASES*************************** */
            else if (intent == 'Recovery_cases') {
                logger.log('************************************* Recovery_cases  *********************************');
                context.sendActivity({ type: 'typing' });
                let a = []
                var entities = recognizerResult.luisResult.entities;
                var total = await requestPost("India")
                if (entities != "") {
                    console.log(entities[0].entity)
                    var Name = entities[0].entity;

                    entities.forEach(entity => {
                        if (entity.type == "builtin.geographyV2.countryRegion" || entity.type == "builtin.geographyV2.state" || entity.type == "builtin.datetimeV2.date" || entity.type == "State" || entity.type == "builtin.geographyV2.city") {
                            a.push(entity.type)
                        }
                        console.log(a)
                    })
                }
                console.log('-----------------------------------', a)
                if (a.includes("builtin.geographyV2.countryRegion") && (a.includes("builtin.datetimeV2.date"))) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST  మొత్తం కోలుకొన్న వారి సంఖ్య " + data.data.delta.recovered + " కి చేరుకుంది");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.delta.recovered + " recoveries reported " + data.data.lastupdatedtime.split(" ")[0] + " in India as of " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में आज" + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " तक कुल " + data.data.delta.recovered + " वसूली दर्ज की गई है")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }

                } else if (a.includes("builtin.geographyV2.countryRegion")) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం కోలుకొన్న వారి సంఖ్య " + data.data.recovered + " కి చేరుకుంది");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.recovered + " total recoveries reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में " + data.data.recovered + " कुल वसूलियाँ हैं, जिन्हें " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " के रूप में रिपोर्ट किया गया है")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                // if (a.includes("builtin.geographyV2.countryRegion" && !(a.includes("builtin.datetimeV2.date")))) {
                //     var data = await requestPost("India")
                //     console.log(data)
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("భారతదేశంలో" + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం కోలుకొన్న వారి సంఖ్య " + data.data.recovered + " కి చేరుకుంది");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.recovered + " total recoveries reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("भारत में " + data.data.recovered + " कुल वसूलियाँ हैं, जिन्हें " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " के रूप में रिपोर्ट किया गया है")
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }
                // }
                else if (a.includes("builtin.geographyV2.state") && !(a.includes("builtin.datetimeV2.date"))) {
                    console.log("Inside else case cour", Name)
                    var currentData = await getStateName(Name)
                    var data = await requestPost(currentData)
                    var aa = await getStatelanguauge(data.data.state, "en")
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(aa[1] + " లో" + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి " + data.data.recovered + " మంది కోలుకున్నట్లు గా నివేదించారు మరియు మన భారతదేశంలో మొత్తం కోలుకొన్న వారి సంఖ్య" + total.data.recovered + " కి చేరుకుంది");
                        else if (language_detect == 'en') {
                            await context.sendActivity(data.data.recovered + " recoveries were reported in " + aa[0] + " and we have total of " + total.data.recovered + " recoveries in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (language_detect == 'hi')
                            await context.sendActivity(aa[2] + " में " + data.data.recovered + " वसूली दर्ज की गई हैं और हमारे पास भारत में " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST तक कुल" + total.data.recovered + " वसूली है")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                else if ((a.includes("builtin.datetimeV2.date")) && (a.includes("builtin.geographyV2.state"))) {
                    console.log(entities)
                    var name, date;
                    entities.forEach(d => {
                        if (d.type == 'builtin.datetimeV2.date')
                            date = d.entity
                        if (d.type == 'builtin.geographyV2.state')
                            name = d.entity
                    })

                    var currentData = await getStateName(name)
                    console.log(currentData)
                    var data = await requestPost(currentData)

                    var aa = await getStatelanguauge(data.data.state, "en")
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(aa[1] + " లో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + "కి'" + data.data.delta.recovered + " మంది కోలుకున్నట్లు గా నివేదించారు మరియు మన భారతదేశంలో మొత్తం కోలుకొన్న వారి సంఖ్య " + total.data.recovered + " కి చేరుకుంది");
                        if (language_detect == 'en') {
                            await context.sendActivity(data.data.delta.recovered + "recoveries were reported " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + "in " + aa[0] + " and we have a total of " + total.data.recovered + " recoveries in India")
                        }
                        else if (language_detect == 'hi')
                            await context.sendActivity(aa[2] + "में " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST " + data.data.delta.recovered + " वसूलियां दर्ज की गईं और भारत में हमारे कुल 1251 वसूल किए गए")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                // else if ((a.includes("builtin.datetimeV2.date")) && !(a.includes("State"))) {
                //     var data = await requestPost("India")
                //     console.log(data)
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("భారతదేశంలో" + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం కోలుకొన్న వారి సంఖ్య " + data.data.recovered + " కి చేరుకుంది");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.recovered + " total recoveries reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("भारत में " + data.data.recovered + " कुल वसूलियाँ हैं, जिन्हें " + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " के रूप में रिपोर्ट किया गया है")
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }
                // }
                // else if (a.includes("builtin.geographyV2.countryRegion" && (a.includes("builtin.datetimeV2.date")))) {
                //     var data = await requestPost("India")
                //     console.log(data)
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("భారతదేశంలో" + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST  మొత్తం కోలుకొన్న వారి సంఖ్య " + data.data.delta.recovered + " కి చేరుకుంది");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.delta.recovered + " recoveries reported " + data.data.lastupdatedtime.split(" ")[0] + " in India as of " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("भारत में आज" + data.data.lastupdatedtime.split(" ")[0] + "" + data.data.lastupdatedtime.split(" ")[1] + " IST" + " तक कुल " + data.data.delta.recovered + " वसूली दर्ज की गई है")
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }
                // }
                else if (a.includes("builtin.geographyV2.city")) {
                    if (language_detect == 'en') {
                        await context.sendActivity("I’m sorry, that is not a state in India")
                    }
                    if (language_detect == 'te') {
                        await context.sendActivity("క్షమించండి, అది భారతదేశంలో ఉన్న రాష్ట్రం కాదు")
                    }
                    if (language_detect == 'hi') {
                        await context.sendActivity("मुझे खेद है, यह भारत का राज्य नहीं है")
                    }
                    context.sendActivity({ type: 'typing' });
                    await timeout()

                    Details.intent = intent
                    console.log("Details", Details)
                    return Details;
                }
                else {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()
                            Details.intent = intent
                            console.log("Details", Details)
                            return Details;
                        }
                        else if (language_detect == 'en') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                            return Details;
                        }
                        else if (language_detect == 'hi') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()
                            Details.intent = intent
                            console.log("Details", Details)
                            return Details;
                        }
                    }
                    else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }

                return Details;
            }

            //*****************************************DEATH CASES*********************************** */

            else if (intent == 'Death_cases') {
                logger.log('*************************************  Death_cases *********************************');
                context.sendActivity({ type: 'typing' });
                let a = []
                var entities = recognizerResult.luisResult.entities;
                var total = await requestPost("India")
                if (entities != "") {
                    console.log(entities[0].entity)
                    var Name = entities[0].entity;

                    entities.forEach(entity => {
                        if (entity.type == "builtin.geographyV2.countryRegion" || entity.type == "builtin.geographyV2.state" || entity.type == "builtin.datetimeV2.date" || entity.type == "builtin.geographyV2.city") {
                            a.push(entity.type)
                        }
                        console.log(a)
                    })
                }
                if (a.includes("builtin.geographyV2.countryRegion") && (a.includes("builtin.datetimeV2.date"))) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో నేటికి " + data.data.lastupdatedtime.split(" ")[0] + "  మొత్తం మరణాల సంఖ్య " + data.data.delta.deaths + " కి చేరుకున్నాయి");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.delta.deaths + " deaths reported " + data.data.lastupdatedtime.split(" ")[0] + " in India as of " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में आज  " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST तक " + data.data.delta.deaths + " मौतें हुई हैं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }

                } else if (a.includes("builtin.geographyV2.countryRegion")) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం మరణాల సంఖ్య " + data.data.deaths + " కి చేరుకున్నాయి");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.deaths + " total deaths reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में " + data.data.deaths + " कुल मौतें " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " हैं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                // if (a.includes("builtin.geographyV2.countryRegion" && !(a.includes("builtin.datetimeV2.date")))) {
                //     var data = await requestPost("India")
                //     console.log(data)
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("భారతదేశంలో" + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం మరణాల సంఖ్య " + data.data.deaths + " కి చేరుకున్నాయి");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.deaths + " total deaths reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("भारत में" + data.data.deaths + " कुल मौतें " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " हैं")
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }

                // }
                else if (a.includes("builtin.geographyV2.state") && !(a.includes("builtin.datetimeV2.date"))) {
                    console.log("Inside else case cour", Name)
                    var currentData = await getStateName(Name)
                    var data = await requestPost(currentData)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(data.data.state + "లో " + data.data.deaths + " మరణాలు నమోదయ్యాయి మరియు" + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి భారతదేశంలో మొత్తం మరణాల సంఖ్య " + total.data.deaths + " కి చేరుకున్నాయి");
                        else if (language_detect == 'en') {
                            await context.sendActivity(data.data.deaths + " deaths were reported in " + data.data.state + " and we have a total of " + total.data.deaths + " deaths in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (language_detect == 'hi')
                            await context.sendActivity(data.data.deaths + "मौतें " + data.data.deaths + " में दर्ज की गईं और हमारे पास भारत में कुल " + total.data.deaths + " मौतें हैं" + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }

                }
                else if ((a.includes("builtin.datetimeV2.date")) && (a.includes("builtin.geographyV2.state"))) {
                    console.log(entities)
                    var name, date;
                    entities.forEach(d => {
                        if (d.type == 'builtin.datetimeV2.date')
                            date = d.entity
                        if (d.type == 'builtin.geographyV2.state')
                            name = d.entity
                    })

                    var currentData = await getStateName(name)
                    console.log(currentData)
                    var data = await requestPost(currentData)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity(data.data.state + "లో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " కి " + data.data.delta.deaths + " మరణాలు నమోదయ్యాయి మరియు మన భారతదేశంలో  మొత్తం మరణాల సంఖ్య " + total.data.deaths + " కి చేరుకున్నాయి");
                        if (language_detect == 'en') {
                            await context.sendActivity(data.data.delta.deaths + " deaths were reported " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " in " + data.data.state + " and we have a total of " + total.data.deaths + " deaths in India")
                        }
                        else if (language_detect == 'hi')
                            await context.sendActivity(data.data.delta.deaths + " मौते " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " को " + data.data.state + " में दर्ज की गईं और भारत में कुल " + total.data.deaths + " मौतें हुईं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }


                }
                else if ((a.includes("builtin.datetimeV2.date")) && !(a.includes("State"))) {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te')
                            await context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " నాటికి మొత్తం మరణాల సంఖ్య " + data.data.deaths + " కి చేరుకున్నాయి");
                        else if (language_detect == 'en')
                            await context.sendActivity("There are " + data.data.deaths + " total deaths reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        else if (language_detect == 'hi')
                            await context.sendActivity("भारत में " + data.data.deaths + " कुल मौतें " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST" + " हैं")
                    } else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                // else if (a.includes("builtin.geographyV2.countryRegion" && (a.includes("builtin.datetimeV2.date")))) {
                //     var data = await requestPost("India")
                //     console.log(data)
                //     if (data.status) {
                //         if (language_detect == 'te')
                //             await context.sendActivity("co vid 19 india info");
                //         else if (language_detect == 'en')
                //             await context.sendActivity("There are " + data.data.delta.deaths + "  reported " + data.data.lastupdatedtime.split(" ")[0] + " in India as of " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                //         else if (language_detect == 'hi')
                //             await context.sendActivity("co vid 19 india info")
                //     } else {
                //         await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                //     }
                // }
                else {
                    var data = await requestPost("India")
                    console.log(data)
                    if (data.status) {
                        if (language_detect == 'te') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                        }
                        else if (language_detect == 'en') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()

                            Details.intent = intent
                            console.log("Details", Details)
                            return Details;
                        }
                        else if (language_detect == 'hi') {
                            context.sendActivity({ type: 'typing' });
                            await timeout()
                            Details.intent = intent
                            console.log("Details", Details)
                            return Details;
                        }
                    }
                    else {
                        await context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    }
                }
                return Details;

            }
            else if (intent == 'Telephone_Helpline') {
                console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                let a = []

                let Name
                var entities = recognizerResult.luisResult.entities;
                if (entities != "") {
                    console.log(entities[0].entity)
                    Name = entities[0].entity;

                    entities.forEach(entity => {
                        if (entity.type == "builtin.geographyV2.state") {
                            a.push(entity.type)
                        }
                        console.log(a)
                    })

                }
                if (a.includes("builtin.geographyV2.state")) {
                    var phdata = await getPhone(Name)
                    console.log(phdata, Name)
                    var aa = await getStatelanguauge(phdata.data.name, "en")
                    if (phdata.data.status) {
                        console.log(a)
                        if (language_detect == 'en') {
                            await context.sendActivity("The helpline number for " + phdata.data.name + " is " + phdata.data.phone + "");
                        }
                        else if (language_detect == 'te') {
                            await context.sendActivity(aa[1] + " యొక్క హెల్ప్‌లైన్ నంబరు " + phdata.data.phone)
                        }
                        else if (language_detect == 'hi') {
                            await context.sendActivity(aa[2] + " के लिए हेल्पलाइन नंबर " + phdata.data.phone + " है")
                        }
                    } else {
                        if (language_detect == 'en') {
                            await context.sendActivity("Okay. Do you need help with anything else?");
                        }
                        else if (language_detect == 'te') {
                            await context.sendActivity("సరే. మీకు మరేదైనా సహాయం చేయగలనా?")
                        }
                        else if (language_detect == 'hi') {
                            await context.sendActivity("ठीक है। क्या आपको किसी और चीज़ की मदद चाहिए?")
                        }
                    }
                    return Details;
                }
                else {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    Details.intent = intent
                    console.log("Details", Details)
                    logger.log('*************************************  STATE_INTENT *********************************');
                    return Details;
                }
            }
            else if (intent == 'Covid-19_Testplace') {
                console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                let aa = []
                var entities = recognizerResult.luisResult.entities;
                if (entities != "") {
                    console.log(entities[0].entity)
                    var Name = entities[0].entity;

                    entities.forEach(entity => {
                        if (entity.type == "builtin.geographyV2.state" || entity.type == "builtin.geographyV2.city") {
                            aa.push(entity.type)
                        }
                        console.log(aa)
                    })

                }
                if (aa.includes("builtin.geographyV2.city")) {
                    if (language_detect == "en") {
                        await context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");

                    }
                    if (language_detect == "te") {

                        await context.sendActivity("క్షమించండి, అది భారతదేశంలో ఉన్న రాష్ట్రం కాదు - దయచేసి సరైన రాష్ట్రాని పేర్కొనండి.");

                    }
                    if (language_detect == "hi") {

                        await context.sendActivity("मुझे खेद है, यह भारत का राज्य नहीं है - कृपया सही राज्य बताएं।")
                    }
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    Details.intent = intent
                    console.log("Details", Details)
                    logger.log('*************************************  STATE_INTENT *********************************');
                    return Details;
                }
                if (aa.includes("builtin.geographyV2.state")) {
                    if (language_detect == "en") {
                        var sname = await getStateName(entities[0].entity)
                        var testcenters = await getTestCenter(sname, "English")
                        if (testcenters.status && testcenters.data[0].center.length != 0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)

                            })
                            var a = await getStatelanguauge(sname, "en")
                            console.log(a, "555555555555555555555555555555555555555555555555555555555")
                            await context.sendActivity("We have the following testing centers in " + sname + ",");
                            await context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });

                        } else {
                            await context.sendActivity("Unfortunately there are no testing centers for COVID-19 currently available in " + sname);

                        }
                    }

                    //*********************************Telugu********************************************* */
                    if (language_detect == "te") {
                        var sname = await getStateName(entities[0].entity)
                        var testcenters = await getTestCenter(sname, "Telugu")
                        // var a = await getStatelanguauge(sname, "en")
                        if (testcenters.status && testcenters.data[0].center.length != 0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "మ్యాప్ లింక్", value: test.mapLink }]))
                                cards.push(card)

                            })
                            var a = await getStatelanguauge(sname, "te")

                            await context.sendActivity(a + " లో ఈ క్రింది పరీక్షా కేంద్రాలు ఉన్నాయి,");
                            await context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });

                        } else {
                            var a = await getStatelanguauge(sname, "te")
                            console.log(a, "888888888888888888888888888888888888")
                            await context.sendActivity("దురదృష్టవశాత్తు ప్రస్తుతం " + a[1] + " లో COVID-19 కోసం ఏ పరీక్షా కేంద్రాలు అందుబాటులో లేవు");

                        }
                    }
                    //*********************************************Hindi**************************************** */
                    if (language_detect == "hi") {
                        var sname = await getStateName(entities[0].entity)
                        var testcenters = await getTestCenter(sname, "Hindi")
                        var a = await getStatelanguauge(sname, "en")
                        if (testcenters.status && testcenters.data[0].center.length != 0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "नक्शा लिंक", value: test.mapLink }]))
                                cards.push(card)

                            })
                            var a = await getStatelanguauge(sname, "hi")
                            console.log(a, "888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888")
                            await context.sendActivity("हमारे पास " + a[2] + " में निम्नलिखित परीक्षण केंद्र हैं,");
                            await context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });

                        } else {
                            await context.sendActivity(" दुर्भाग्य से " + a[2] + " में वर्तमान में COVID-19 के लिए कोई परीक्षण केंद्र नहीं हैं");

                        }
                    }
                    return Details
                }
                else {
                    context.sendActivity({ type: 'typing' });
                    await timeout()
                    Details.intent = intent
                    console.log("Details", Details)
                    logger.log('*************************************  STATE_INTENT *********************************');
                    return Details;
                }
            }

        }
        catch (err) {
            logger.warn(`LUIS Exception: ${err} Check your LUIS configuration`);
        }
    }
    /*static parseCompositeEntity(result, compositeName, entityName) {
        const compositeEntity = result.entities[compositeName];
        if (!compositeEntity || !compositeEntity[0]) return undefined;

        const entity = compositeEntity[0][entityName];
        if (!entity || !entity[0]) return undefined;

        const entityValue = entity[0][0];
        return entityValue;
    }

    static parseDatetimeEntity(result) {
        const datetimeEntity = result.entities['datetime'];
        if (!datetimeEntity || !datetimeEntity[0]) return undefined;

        const timex = datetimeEntity[0]['timex'];
        if (!timex || !timex[0]) return undefined;

        const datetime = timex[0].split('T')[0];
        return datetime;
    }*/
}

module.exports.LuisHelper = LuisHelper;
//module.exports.StateDialog = StateDialog;
// {
//     status: true,
//     data: {
//       _id: '5e85dac81e54913ab45df943',
//       active: '130',
//       confirmed: '132',
//       deaths: '0',
//       delta: { active: 21, confirmed: 21, deaths: 0, recovered: 0 },
//       lastupdatedtime: '02/04/2020 10:52:36',
//       recovered: '2',
//       state: 'Andhra Pradesh',
//       __v: 0
//     }
//   }