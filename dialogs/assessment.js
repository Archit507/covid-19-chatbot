// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
const { ConfirmPrompt, ChoicePrompt, TextPrompt, WaterfallDialog, ComponentDialog } = require('botbuilder-dialogs');
const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');
const { DateResolverDialog } = require('./dateResolverDialog');
const Card = require('../cards')
const { CardFactory } = require('botbuilder-core');
const CHOICE_PROMPT = 'ChoicePrompt';
const CONFIRM_PROMPT = 'ConfirmPrompt';
const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';

class Assessment extends ComponentDialog {
    constructor(id) {
        super(id || 'Assessment');
        this.addDialog(new TextPrompt(TEXT_PROMPT, this.textvalidator))
            .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.askConfirmation.bind(this),
                this.validate.bind(this),
                this.gender.bind(this),
                this.age.bind(this),
                this.state.bind(this),
                this.symptoms.bind(this),
                this.history.bind(this),
                this.travel.bind(this),
                this.area.bind(this),
                this.contact.bind(this),
                this.finalStep.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    /**
     * If a destination city has not been provided, prompt for one.
     */

    async askConfirmation(stepContext) {
        // context.sendActivity({ type: 'typing' });    
        console.log("heyyyyy")
        await stepContext.context.sendActivity("The main aim of this Coronavirus (COVID-19) self-assessment is to help you take proper medical decisions per guidelines provided by WHO. Please note that I’m not a medical expert and this is just a COVID-19 symptom checker, so for any life-threatening cases please consult a medical professional or call 911")
        return await stepContext.prompt(CONFIRM_PROMPT, { prompt: "Do you agree to the terms?" });

        // stepContext.prompt(CHOICE_PROMPT);

    }

    async validate(stepContext) {
        console.log(stepContext.result)
        if (stepContext.result) {
            await stepContext.prompt(TEXT_PROMPT, { prompt: "Let us get started!" });
            return await stepContext.continueDialog();
        }
        else {
            await stepContext.context.sendActivity("Your consent is required to move forward with the self-assessment. Please click on Yes to agree our terms")
            return await stepContext.endDialog();
        }
    }

    async gender(stepContext) {
        const bookingDetails = stepContext.options;
        var suggest = CardFactory.heroCard('What is your Gender?', [], ['Male', 'Female', 'Non-Binary'])
        return await stepContext.prompt(TEXT_PROMPT, { prompt: { attachments: [suggest] } });

    }

    /**
     * If an origin city has not been provided, prompt for one.
     */
    async age(stepContext) {
        const bookingDetails = stepContext.options;
        console.log("*********************************", stepContext.result)
        // Capture the response to the previous step's prompt
        bookingDetails.gender = stepContext.result;
        var suggest = CardFactory.heroCard('What is your Age?', [], ['Below 18', 'Between 18 and 60', 'Above 60'])
        return await stepContext.prompt(TEXT_PROMPT, { prompt: { attachments: [suggest] } });

    }

    async state(stepContext) {
        const bookingDetails = stepContext.options;
        // await stepContext.context.sendActivity({ attachments: [CardFactory.adaptiveCard(Card["states"])] })
        // return await stepContext.endDialog();
        // Capture the response to the previous step's prompt
        bookingDetails.age = stepContext.result;

        return await stepContext.prompt(TEXT_PROMPT, { prompt: "state" });

    }

    async symptoms(stepContext) {
        const bookingDetails = stepContext.options;

        // Capture the response to the previous step's prompt
        bookingDetails.state = stepContext.result;
        // return await stepContext.prompt(TEXT_PROMPT, { prompt:{attachments: [CardFactory.adaptiveCard(Card["symptoms"])]} });
        await stepContext.context.sendActivity({ attachments: [CardFactory.adaptiveCard(Card["symptoms"])] })
        return await stepContext.prompt(TEXT_PROMPT, { prompt: "" });


    }

    async history(stepContext) {
        const bookingDetails = stepContext.options;
        console.log(stepContext.context.activity.value)
        // Capture the response to the previous step's prompt Do you have a medical history of any of the following conditions?
        bookingDetails.symptoms = stepContext.context.activity.value;

        await stepContext.context.sendActivity({ attachments: [CardFactory.adaptiveCard(Card["medical"])] })
        return await stepContext.prompt(TEXT_PROMPT, { prompt: "" });

    }


    async travel(stepContext) {
        const bookingDetails = stepContext.options;
        console.log(stepContext.context.activity.value)
        // Capture the response to the previous step's prompt
        bookingDetails.history = stepContext.context.activity.value;

        return await stepContext.prompt(CONFIRM_PROMPT, { prompt: "Have you traveled internationally in the last month?" });

    }

    async area(stepContext) {
        const bookingDetails = stepContext.options;

        // Capture the response to the previous step's prompt
        bookingDetails.travel = stepContext.result;

        return await stepContext.prompt(CONFIRM_PROMPT, { prompt: "Is COVID-19 widespread in your area?" });

    }


    async contact(stepContext) {
        const bookingDetails = stepContext.options;

        // Capture the response to the previous step's prompt
        bookingDetails.area = stepContext.result;


        return await stepContext.prompt(CONFIRM_PROMPT, { prompt: " Have you been in contact with anybody with COVID-19?" });

    }

    /**
     * Complete the interaction and end the dialog.
     * {
  gender: 'Male',
  age: 'Below 18',
  state: 'Novi',
  symptoms: { Category: 'Sore Throat,Runny Nose' },
  history: { Category: 'None' },
  travel: true,
  area: true,
  contact: true
}
     */
    async finalStep(stepContext) {
        const bookingDetails = stepContext.options;
        bookingDetails.contact = stepContext.result;


        console.log(stepContext.options)
        if (stepContext.result === true) {
            //If the user’s age is between 18 and 60 (or) answers no for all the questions, 
            if (stepContext.options.age == "Between 18 and 60" || stepContext.options.symptoms.Category == "None" && stepContext.options.symptoms.history == "None" && stepContext.options.travel && stepContext.options.area && stepContext.options.contact) {
                await stepContext.context.sendActivity("Please call your primary care physician and get tested for COVID-19 immediately")
                return await stepContext.endDialog();
            }
            // If the user’s age is below 18 (or) Above 60 (or) has a history of a medical condition,
            // Result: You are currently showing no symptoms but have relevant conditions for complications should you become infected with the Coronavirus. Please stay at home and practice social distancing
            if ((stepContext.options.age == "Below 18" || stepContext.options.age == "Above 60") || stepContext.options.symptoms.Category == "None") {
                await stepContext.context.sendActivity("You are currently showing no symptoms but have relevant conditions for complications should you become infected with the Coronavirus. Please stay at home and practice social distancing.")
                return await stepContext.endDialog();

            }
            //         If the user selects any of the symptoms instead of none,
            // Result: You have relevant symptoms related to the Coronavirus, self-isolate to avoid spread and take prescribed medications. Should you feel worse, contact your primary care doctor
            if (stepContext.options.symptoms.Category != "None") {
                await stepContext.context.sendActivity("You are currently showing no symptoms but have relevant conditions for complications should you become infected with the Coronavirus. Please stay at home and practice social distancing.")
                return await stepContext.endDialog();
            }

            //If the user selects any of the symptoms instead of none (and) says yes to any one of the last 3 questions,
            // Result: You have relevant symptoms related to the Coronavirus, self-isolate to avoid spread and take prescribed medications. Should you feel worse, contact your primary care doctor
            if (stepContext.options.symptoms.Category != "None" && (stepContext.options.travel || stepContext.options.area || stepContext.options.contact)) {
                await stepContext.context.sendActivity("You have relevant symptoms related to the Coronavirus, self-isolate to avoid spread and take prescribed medications. Should you feel worse, contact your primary care doctor.")
                return await stepContext.endDialog();
            }
            // If the user selects any of the symptoms instead of none (and) says yes to more than one of the last 3 questions,
            // Result: Please call your primary care physician and get tested for COVID-19 immediately
            if (stepContext.options.symptoms.Category != "None" && (stepContext.options.travel || stepContext.options.area )&&(stepContext.options.contact || stepContext.options.area )) {
                await stepContext.context.sendActivity("Please call your primary care physician and get tested for COVID-19 immediately.")
                return await stepContext.endDialog();
            }
            // If the user selects any of the symptoms instead of none (and) selects a condition (and) says yes to any one of the last 3 questions,
            // Result: Please call your primary care physician and get tested for COVID-19 immediately
            if (stepContext.options.symptoms.Category != "None" && stepContext.options.history.Category != "None"&& (stepContext.options.travel || stepContext.options.area || stepContext.options.contact)) {
                await stepContext.context.sendActivity("Please call your primary care physician and get tested for COVID-19 immediately.")
                return await stepContext.endDialog();
            }
        } else {
            return await stepContext.endDialog();
        }
    }
    async textvalidator(promptcontext) {
        return true;
    }

}

module.exports.Assessment = Assessment;
