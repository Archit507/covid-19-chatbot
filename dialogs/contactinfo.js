// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// ==========================================  IMPORT STATEMENTS =========================================//
const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
//var MiscResponse = require('../Responses/misc_response.json')
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog, ConfirmPrompt } = require('botbuilder-dialogs');
// const { CardFactory } = require('botbuilder-core');
const { CardFactory, ActionTypes, MessageFactory } = require('botbuilder-core');

const { Language_name } = require('./mainDialog.js')
const TEXT_PROMPT = 'textPrompt';
const CONFIRM_PROMPT = 'confirmPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
var fuzz = require('fuzzball')
var states = require('../states.json');
var numbers = require('../Telephone.json')
var stateLanguage = require('../statelanguage.json')

async function getStatelanguauge(name, lang) {
    data = ""
    return new Promise((resolve) => {

        stateLanguage.states.forEach(element => {
            if (element[0] == name) {
                if (lang == "te") {
                    data = element
                }
                else if (lang == "en") {
                    data = element
                } else {
                    data = element
                }
            }

        })
        console.log(name, "6666666666666666666666666666666666666666666666", lang, data)
        resolve(data)

    });
}
async function getPhone(statename) {
    var prev = 0
    return new Promise((resolve) => {
        states.states.forEach(element => {
            fuzz_ratio = fuzz.ratio(statename, element);
            if (fuzz_ratio >= 60 && prev < fuzz_ratio) {
                prev = fuzz_ratio
                // console.log(fuzz_ratio, element)
                numbers.forEach(ele => {
                    if (ele.name == element && prev <= fuzz_ratio) {
                        prev = fuzz_ratio
                        phone = ele.phone
                        resolve({ data: { "phone": phone, "name": ele.name, "status": true } })
                    }
                })
            }
        });

    });
}

// ====================================  MAIN CLASS STARTS =========================================//
class ContactInfo extends ComponentDialog {
    constructor(id) {
        super(id || 'ContactInfo');
        this.addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.statePrompt.bind(this),
                this.finalStep.bind(this),
                this.Statename.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }
    async statePrompt(stepContext) {
        //const statedetails = stepContext.options;
        console.log("Inside state waterfall", stepContext)
        //var lang_name = stepContext.context.activity.text;
        //console.log(lang_name)
        if (stepContext.context.activity.language == "en") {
            await stepContext.context.sendActivity("The central helpline number for COVID-19 is +91-11-23978046 (or) 1075 for India. ");
            var reply = MessageFactory.suggestedActions(['Yes', 'No'], 'Would you like the phone number for your state?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
            // return await stepContext.prompt(CONFIRM_PROMPT, 'Would you like the phone number for your state?');
        }
        if (stepContext.context.activity.language == "te") {
            await stepContext.context.sendActivity("భారతదేశపు COVID-19 కేంద్ర హెల్ప్‌లైన్ నంబర్ + 91-11-23978046 (లేదా) 1075.");
            var reply = MessageFactory.suggestedActions(['అవును', 'కాదు'], 'మీరు మీ రాష్ట్రము యొక్క ఫోన్ నంబర్ తెలుసుకోవలనుకుంటున్నారా?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
            // return await stepContext.prompt(CONFIRM_PROMPT, 'మీరు మీ రాష్ట్రము యొక్క ఫోన్ నంబర్ తెలుసుకోవలనుకుంటున్నారా?');
        }
        if (stepContext.context.activity.language == "hi") {
            await stepContext.context.sendActivity("COVID-19 के लिए केंद्रीय हेल्पलाइन नंबर भारत के लिए + 91-11-23978046 (या) 1075 है।");
            var reply = MessageFactory.suggestedActions(['हाँ', 'नहीं'], 'क्या आप अपने राज्य के लिए फ़ोन नंबर जानना चाहते हैं?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
            // return await stepContext.prompt(CONFIRM_PROMPT, 'क्या आप अपने राज्य के लिए फ़ोन नंबर जानना चाहते हैं?');
        }
    }


    async finalStep(stepContext) {
        console.log(stepContext.result, "333333333333333333333333333333333333333333333333333")
        if (stepContext.result=="Yes" || stepContext.result=="Of course") {
            if (stepContext.context.activity.language == "en") {
                return await stepContext.prompt(TEXT_PROMPT, 'Which state would you like the information for?');
            }
            if (stepContext.context.activity.language == "te") {
                return await stepContext.prompt(TEXT_PROMPT, 'మీరు ఏ రాష్ట్రానికి సంబంధించిన సమాచారం తెలుసుకోవాలనుకుంటున్నాను?');
            }
            if (stepContext.context.activity.language == "hi") {
                return await stepContext.prompt(TEXT_PROMPT, 'क्या आप अपने राज्य के लिए फ़ोन नंबर जानना चाहते हैं?');
            }

        }
        else{
            console.log(stepContext.context.activity.language)
                if (stepContext.context.activity.language == "es") {
                await stepContext.context.sendActivity("Okay, glad that I was able to help you");
                return await stepContext.endDialog();
            }
            if (stepContext.context.activity.language == "te") {
                await stepContext.context.sendActivity("సరే, నేను మీకు సహాయం చేయగలిగినందుకు ఆనందంగా ఉంది");
               return await stepContext.endDialog();
            }
            if (stepContext.context.activity.language == "hi") {
                await stepContext.context.sendActivity("ठीक है, खुशी है कि मैं आपकी मदद करने में सक्षम था");
               return await stepContext.endDialog();
            }
        }

        // if (stepContext.result=="Of course") {
        //     await stepContext.context.sendActivity("సరే, నేను మీకు సహాయం చేయగలిగినందుకు ఆనందంగా ఉంది");
        //        // return await stepContext.endDialog();
        //     if (stepContext.context.activity.language == "te") {
        //         return await stepContext.prompt(TEXT_PROMPT, 'మీరు ఏ రాష్ట్రానికి సంబంధించిన సమాచారం తెలుసుకోవాలనుకుంటున్నాను?');
        //     }
        //     if (stepContext.context.activity.language == "hi") {
        //         return await stepContext.prompt(TEXT_PROMPT, 'ठीक है, खुशी है कि मैं आपकी मदद करने में सक्षम था');
        //     }

        // }
        // else {
           

        //     if (stepContext.context.activity.language == "en") {
        //         await stepContext.context.sendActivity("Okay, glad that I was able to help you");
        //         return await stepContext.endDialog();
        //     }
        //     if (stepContext.context.activity.language == "te") {
        //         await stepContext.context.sendActivity("సరే, నేను మీకు సహాయం చేయగలిగినందుకు ఆనందంగా ఉంది");
        //         return await stepContext.endDialog();
        //     }
        //     if (stepContext.context.activity.language == "hi") {
        //         await stepContext.context.sendActivity("ठीक है। क्या आपको किसी और चीज़ की मदद चाहिए?");
        //         return await stepContext.endDialog();
        //     }

        // }

    }


    async Statename(stepContext) {
        console.log("ffffffffffffffffiiiiiiiiiiiiiinaaaaaaaaaaaaaaaallllllllllllllllll")
        var phdata = await getPhone(stepContext.result)
        console.log(phdata, phdata.data.name)
        var a = await getStatelanguauge(phdata.data.name, "te")
        if (phdata.data.status) {
            console.log(a)
            if (stepContext.context.activity.language == 'en') {
                await stepContext.context.sendActivity("The helpline number for " + phdata.data.name + " is " + phdata.data.phone + "");
            }
            else if (stepContext.context.activity.language == 'te') {
                await stepContext.context.sendActivity(a[1] + " యొక్క హెల్ప్‌లైన్ నంబరు " + phdata.data.phone)
            }
            else if (stepContext.context.activity.language == 'hi') {
                await stepContext.context.sendActivity(a[2] + " के लिए हेल्पलाइन नंबर " + phdata.data.phone + " है")
            }
        } else {
            if (stepContext.context.activity.language == 'en') {
                await stepContext.context.sendActivity("Okay, glad that I was able to help you");
            }
            else if (stepContext.context.activity.language == 'te') {
                await stepContext.context.sendActivity("సరే, నేను మీకు సహాయం చేయగలిగినందుకు ఆనందంగా ఉంది")
            }
            else if (stepContext.context.activity.language == 'hi') {
                await stepContext.context.sendActivity("ठीक है, खुशी है कि मैं आपकी मदद करने में सक्षम था")
            }
        }
        return await stepContext.endDialog();
    }

}

module.exports.ContactInfo = ContactInfo;
