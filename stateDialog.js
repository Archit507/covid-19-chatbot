// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// ==========================================  IMPORT STATEMENTS =========================================//
const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
//var MiscResponse = require('../Responses/misc_response.json')
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { ActivityHandler, MessageFactory } = require('botbuilder');

const { CardFactory } = require('botbuilder-core');
const { Language_name } = require('./dialogs/mainDialog.js')
const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const states = require('./states.json')
const fuzz = require('fuzzball')

function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}
// ====================================  MAIN CLASS STARTS =========================================//
class StateDialog extends ComponentDialog {
    constructor(id) {
        super(id || 'StateDialog');
        this.addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.statePrompt.bind(this),
                this.finalStep.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }
    async statePrompt(stepContext) {
        const statedetails = stepContext.options;
        //console.log("Inside state waterfall", stepContext)
        var lang_name = stepContext.context.activity.text;

        console.log(lang_name)
        if (lang_name == "English") {
            stepContext.context.activity.select = "English"
            console.log(stepContext.context.activity.select)
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("Sure, I will speak with you in English today. You can change the language at any time by just speaking to me in English, తెలుగు (or) हिन्दी.");
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("I can help answer questions about COVID-19, share information around testing centers and helplines in India and more.")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("Any information that is collected during our conversation will not be shared and will be kept confidential.")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            return await stepContext.prompt(TEXT_PROMPT, "To serve you better, can you let me know which state you are from in India?");
        }

        if (lang_name == "Telugu") {
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("ఖచ్చితంగా, నేను ఈ రోజు తెలుగు లో మాట్లాడతాను. “భాష మార్చండి” అని చెప్పడం ద్వారా మీరు ఎప్పుడైనా భాషా సెట్టింగులను మార్చవచ్చు");
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("COVID-19 యొక్క ప్రశ్నలకు సమాధానం ఇవ్వడానికి, భారతదేశంలోని పరీక్షా కేంద్రాలు మరియు హెల్ప్‌లైన్‌ల గురించి సమాచారాన్ని పంచుకోవడానికి నేను సహాయం చేయగలను.")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("మన సంభాషణ సమయంలో సేకరించిన ఏదైనా సమాచారం భాగస్వామ్యం చేయబడదు మరియు గోప్యంగా ఉంచబడుతుంది.")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            return await stepContext.prompt(TEXT_PROMPT, "మీకు సరైన సమాచారం అందించడానికి, మీరు భారతదేశంలో ఏ రాష్ట్రం నుండి వచ్చారో నాకు తెలియజేయగలరా?");
        }

        if (lang_name == "Hindi") {
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("ज़रूर, आज हिन्दी में बोलूंगा। आप 'भाषा बदलें' कहकर कभी भी भाषा सेटिंग बदल सकते हैं");
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            await stepContext.context.sendActivity("मैं COVID -19 के बारे में सवालों के जवाब देने में मदद कर सकता हूं, भारत में परीक्षण केंद्रों और हेल्पलाइनों के बारे में जानकारी साझा कर सकता हूं।")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()

            await stepContext.context.sendActivity("हमारी बातचीत के दौरान एकत्र की गई कोई भी जानकारी साझा नहीं की जाएगी और उसे गोपनीय रखा जाएगा।")
            stepContext.context.sendActivity({ type: 'typing' });
            await timeout()
            return await stepContext.prompt(TEXT_PROMPT, "आपकी बेहतर सेवा करने के लिए, क्या आप मुझे बता सकते हैं कि आप भारत के किस राज्य से हैं");
        }
    }
    async finalStep(stepContext) {
        if(stepContext.context.activity.language == "en")
        {
        console.log("Inside State watwerfall", stepContext.result)
        // var textData = stepContext.result        
        var state;
        var prev=0
        states.states.forEach(element => {
            var fuzz_ratio = fuzz.ratio(stepContext.result  , element);
            if (fuzz_ratio >= 60 && prev<fuzz_ratio) {
                prev=fuzz_ratio;
                console.log(fuzz_ratio, element)
                state=element
            }
        });
        stepContext.context.activity.state=state

        var reply = MessageFactory.suggestedActions(['Questions about COVID-19', 'Testing Center Locations', 'Government Helplines', 'COVID-19 Cases Count'], 'I can help you with the following,');
        await stepContext.context.sendActivity(reply);
        return await stepContext.endDialog();

    }
    if(stepContext.context.activity.language == "te")
    {
    console.log("Inside State watwerfall", stepContext.result)
    var textData = stepContext.result.split(" ")        
    
        var prev=0
        textData.forEach(ele1 => {
            states.states.forEach(element => {
                var fuzz_ratio = fuzz.ratio(ele1, element);
                if (fuzz_ratio >= 60 && prev<fuzz_ratio) {
                    prev=fuzz_ratio;
                    console.log(fuzz_ratio, element)
                    stepContext.context.activity.state=element
                }
            });
        })
    var reply = MessageFactory.suggestedActions(['COVID-19 గురించి ప్రశ్నలు', 'పరీక్ష కేంద్రం', 'ప్రభుత్వ హెల్ప్‌లైన్‌లు', 'COVID-19 కేసుల సంఖ్య'], 'కింది వాటితో నేను మీకు సహాయం చేయగలను,');
    await stepContext.context.sendActivity(reply);
    return await stepContext.endDialog();

}
if(stepContext.context.activity.language == "hi")
    {
    console.log("Inside State watwerfall", stepContext.result)
    var textData = stepContext.result.split(" ")        
    var prev=0
    textData.forEach(ele1 => {
        states.states.forEach(element => {
            var fuzz_ratio = fuzz.ratio(ele1, element);
            if (fuzz_ratio >= 60 && prev<fuzz_ratio) {
                prev=fuzz_ratio;
                console.log(fuzz_ratio, element)
                stepContext.context.activity.state=element
            }
        });
    })

    var reply = MessageFactory.suggestedActions(['COVID-19 के बारे में प्रश्न', 'परीक्षण केंद्र स्थान', 'सरकारी हेल्पलाइन', 'COVID-19 मामलों की गिनती'], 'मैं निम्नलिखित के साथ आपकी मदद कर सकता हूं,');
    await stepContext.context.sendActivity(reply);
    return await stepContext.endDialog();

}

}
}

module.exports.StateDialog = StateDialog;
