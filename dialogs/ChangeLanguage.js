// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// ==========================================  IMPORT STATEMENTS =========================================//
const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
//var MiscResponse = require('../Responses/misc_response.json')
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, ConfirmPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { CardFactory } = require('botbuilder-core');
const { Language_name } = require('../dialogs/mainDialog.js')
const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const states = require('../states.json')
const fuzz = require('fuzzball')
const CONFIRM_PROMPT = 'confirmPrompt';



function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}
// ====================================  MAIN CLASS STARTS =========================================//
class ChangeLanguage extends ComponentDialog {
    constructor(id) {
        super(id || 'DateState');
        this.addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.statePrompt.bind(this),
                this.yesPrompt.bind(this),
                this.finalStep.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }
    async statePrompt(stepContext) {
        //const statedetails = stepContext.options;
        console.log("Inside state waterfall", stepContext)
        var lang_name = stepContext.context.activity.text;

        console.log(lang_name)


        return await stepContext.prompt(CONFIRM_PROMPT, "You have currently selected '"+ stepContext.context.activity.Selectedlanguage+"' . Would you like to change the language to "+stepContext.result+"?");
    }

    async yesPrompt(stepContext) {
        //const statedetails = stepContext.options;
      if(stepContext.result){
        let capability = CardFactory.heroCard('What language would you like to speak with me in?', [], ['English','తెలుగు','हिन्दी'])
        await stepContext.context.sendActivity({ attachments: [capability]});
        stepContext.context.activity.update=true
        return await stepContext.prompt(TEXT_PROMPT, "");

      }else{

        await stepContext.context.sendActivity("Ok. You can change the language by giving 'change Language")

        return await stepContext.endDialog();
      }
    }


    async finalStep(stepContext) {
        console.log("Inside State watwerfall", stepContext.result)
        stepContext.context.activity.update=true
        stepContext.context.activity.Selectedlanguage=stepContext.context.activity.language
        console.log(stepContext.context.activity.Selectedlanguage,"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")      
        await stepContext.context.sendActivity('Thank you, your language changed to '+ stepContext.result)

        return await stepContext.endDialog();

    }
}

module.exports.ChangeLanguage = ChangeLanguage;
