const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { LuisHelper } = require('./luisHelper');
const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';
const { language } = require('../index.js')
const { ActivityHandler, CardFactory, ActionTypes, MessageFactory } = require('botbuilder');

const { StateDialog } = require('../stateDialog.js')

const { ContactInfo } = require('../dialogs/contactinfo.js')
const { TestCenter } = require('../dialogs/TestCenter.js')
const { DateState } = require('../dialogs/Date_State_Dialog.js')
const { RecoverState } = require('../dialogs/Recover_State_Date.js')
const { DeathState } = require('../dialogs/Death_State_Date.js')
const {ChangeLanguage} = require('../dialogs/ChangeLanguage.js')
const STATE_RESPONSE = 'stateDialog';

const CONTACT_INFO = 'ContactInfo'

const CHANGE_LANGUAGE= 'ChangeLanguage'
const TEST_CENTER = 'testcenter'

const DATE_STATE = 'Datestate'

const RECOVER_STATE = 'recoverstate'

const DEATH_STATE = 'deathstate'
//const LUIS_HELPER = 'LuisHelper'
function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}

class MainDialog extends ComponentDialog {
    constructor(logger) {
        super('MainDialog');

        if (!logger) {
            logger = console;
            logger.log('[MainDialog]: logger not passed in, defaulting to console');
        }

        this.logger = logger;

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.
        this.addDialog(new StateDialog(STATE_RESPONSE));
        this.addDialog(new ContactInfo(CONTACT_INFO));
        this.addDialog(new TestCenter(TEST_CENTER));
        this.addDialog(new DateState(DATE_STATE));
        this.addDialog(new RecoverState(RECOVER_STATE));
        this.addDialog(new DeathState(DEATH_STATE));
        this.addDialog(new ChangeLanguage(CHANGE_LANGUAGE));
        this.addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [

            this.actStep.bind(this)

        ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a DialogContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} dialogContext
     */
    async run(context, accessor) {

        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(context);
        const results = await dialogContext.continueDialog();


        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }
    async actStep(stepContext) {
        var User_selcted_lang = stepContext.context._activity.Selectedlanguage
        var Detected_Lang =stepContext.context._activity.language        
        console.log(User_selcted_lang,Detected_Lang)
        // console.log("Inside main dialog stepcontext",User_selcted_lang)
        // if(Detected_Lang ==User_selcted_lang )
        // {
        //     console.log("Inside language detect if")
        // }
        // else(
        //     console.log("Not detected")
        // )
        //if(User_selcted_lang==Detected_Lang){
            // console.log(JSON.parse(stepContext.context.activity.text),JSON.parse(stepContext.context.activity.text).type,"-------------------------------")
        if(stepContext.context.activity.text[0]=='{')
        {
            await stepContext.context.sendActivity("Hello " +stepContext.context.activity.from.name+ "! My name is Aadhya - the COVID-19 Assistance Chatbot for India.")
            var reply = MessageFactory.suggestedActions(['English', 'తెలుగు', 'हिन्दी'], 'What language would you like to speak with me in?');
                await stepContext.context.sendActivity(reply);
            return await stepContext.endDialog();
        }
        else
          if (stepContext.context._activity.text == 'English' || stepContext.context._activity.text == 'Telugu' || stepContext.context._activity.text == 'Hindi') {
                // stepContext.context._activity.select = stepContext.context._activity.text
                // console.log("Inside mian dialog",stepContext.context._activity.select)

                return await stepContext.beginDialog(STATE_RESPONSE)

                // console.log("+++++++++++++++++++++++++++++ Inside main dialog")
                // await stepContext.context.sendActivity(" Sure, we will speak in "+stepContext.context._activity.text+ " today. You can change the language settings at anytime by saying \“Change Language\”")
                // console.log("+++++++++++++++++++++++++++++ Inside main dialog")
                // await stepContext.context.sendActivity({ type: 'typing' });
                // await timeout()
                // await stepContext.context.sendActivity("Welcome - I can help answer questions about COVID-19, share information around testing centers and helplines in India and more. \n\n---\n\n Any information that is collected during our conversation will not be shared and will be kept confidential.")
                // await stepContext.context.sendActivity({ type: 'typing' });
                // await timeout()
                // await stepContext.context.sendActivity("What state are you from?")



                //return await stepContext.endDialog();
            }

            else if (process.env.LuisAppId && process.env.LuisAPIKey && process.env.LuisAPIHostName) {


                // Call LUIS and gather any potential .
                var Details = {}

                Details = await LuisHelper.executeLuisQuery(this.logger, stepContext.context, stepContext);
                console.log("Main dialog", Details)
                //return await stepContext.continueDialog();
                if (Details.intent) {
                    if (Details.intent == "Telephone_Helpline") {
                        // this.logger.log('LUIS extracted these booking details:', Details);
                        return await stepContext.beginDialog(CONTACT_INFO)

                    }
                    else if (Details.intent == "Covid-19_Testplace") {
                        this.logger.log('LUIS extracted these booking details:', Details);
                        return await stepContext.beginDialog(TEST_CENTER)
                    }
                    else if (Details.intent == 'Case_count') {
                        this.logger.log('LUIS extracted these booking details:', Details);
                        return await stepContext.beginDialog(DATE_STATE)
                    }
                    else if (Details.intent == 'Recovery_cases') {
                        this.logger.log('LUIS extracted these booking details:', Details);
                        return await stepContext.beginDialog(RECOVER_STATE)
                    }
                    else if (Details.intent == 'Death_cases') {
                        this.logger.log('LUIS extracted these booking details:', Details);
                        return await stepContext.beginDialog(DEATH_STATE)
                    }
                    //     return await stepContext.beginDialog('confirmation', Details)

                }
                else {
                    console.log("**************************************** Else **********************")
                    return await stepContext.continueDialog();

                }

            }
        //}
        // else{
        //     console.log("**************************************** language selected is not matched **********************")
        //     // stepContext.context.sendActivity("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@language mismatched")
        //     //return await stepContext.beginDialog(CHANGE_LANGUAGE)
        //     // return await stepContext.endDialog();

        // }
    }

}





module.exports.MainDialog = MainDialog;