const { ActivityHandler } = require('botbuilder');
const {MainDialog}= require('../dialogs/mainDialog')
// The accessor names for the conversation data and user profile state property accessors.
const CONVERSATION_DATA_PROPERTY = 'conversationData';
const USER_PROFILE_PROPERTY = 'userProfile';
 
class DialogBot extends ActivityHandler {
    constructor(userState,conversationState,dialog) {
        super();
        // Create the state property accessors for the conversation data and user profile.
        // this.conversationDataAccessor = conversationState.createProperty(CONVERSATION_DATA_PROPERTY);
        this.userProfileAccessor = userState.createProperty(USER_PROFILE_PROPERTY);
        this.dialogState=conversationState.createProperty(CONVERSATION_DATA_PROPERTY);
        // The state management objects for the conversation and user state.
        this.conversationState = conversationState;
        this.userState = userState;
        this.dialog=dialog;
        console.log(userState, "---------------------------------------")
        this.onMessage(async (turnContext, next) => {
            // Get the state properties from the turn context.    
            // userProfile.language="en"        
            // userProfile.state="Andhra Pradesh"
            
            const userProfile = await this.userProfileAccessor.get(turnContext, {});
            if (!userProfile.language) {
                userProfile.language = turnContext.activity.language
                turnContext.activity.Selectedlanguage = userProfile.language
                turnContext.activity.state = userProfile.state
                console.log("on message",turnContext)
            } else {
                turnContext.activity.Selectedlanguage = userProfile.language
                turnContext.activity.state = userProfile.state
                console.log("on message", turnContext)
            }

            

            // if (!userProfile.state) {
            //     userProfile.language = turnContext.activity.language
            //     turnContext.activity.Selectedlanguage = userProfile.language
            //     console.log("on message",turnContext)
            // } else {
            //     turnContext.activity.Selectedlanguage = userProfile.language
            //     console.log("on message", turnContext)
            // }
            
            await this.dialog.run(turnContext,this.dialogState)
            console.log("after dialog runiimggggggggggggggggggggggggggggg",turnContext)
            if( turnContext.activity.update){
                userProfile.language = turnContext.activity.language
            }
            if( turnContext.activity.state){
                userProfile.state = turnContext.activity.state
            }
            await this.userState.saveChanges(turnContext, false);
            await this.conversationState.saveChanges(turnContext, false);
            //await next();
        });
 
        // this.onMembersAdded(async (context, next) => {
        //     const membersAdded = context.activity.membersAdded;
        //     for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
        //         if (membersAdded[cnt].id !== context.activity.recipient.id) {
        //             await context.sendActivity('Welcome to State Bot Sample. Type anything to get started.', context.language);
        //         }
        //     }
        //     // By calling next() you ensure that the next BotHandler is run.
        //     await next();
        // });
    }
    
    /**
     * Override the ActivityHandler.run() method to save state changes after the bot logic completes.
     */
    // async run(context) {
    //     await super.run(context);
    //     // context.language="English"
    //     console.log("in run", context)
        
    //     // Save any state changes. The load happened during the execution of the Dialog.
    //     // await this.conversationState.saveChanges(context, false);
    //     await this.userState.saveChanges(context, false);
    // }
}
 
module.exports.DialogBot = DialogBot;
