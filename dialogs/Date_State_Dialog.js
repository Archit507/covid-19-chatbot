// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// ==========================================  IMPORT STATEMENTS =========================================//
const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
//var MiscResponse = require('../Responses/misc_response.json')
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, ChoicePrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { CardFactory, MessageFactory } = require('botbuilder-core');
const { Language_name } = require('../dialogs/mainDialog.js')
const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const states = require('../states.json')
const fuzz = require('fuzzball')
const CHOICE_PROMPT = 'choiceprompt'
const request = require('request')
const CONFIRM_PROMPT = 'confirmPrompt';

var stateLanguage = require('../statelanguage.json')
function timeout() {
    return new Promise(resolve => {
        setTimeout(resolve, 1500);
    });
}

async function getStatelanguauge(name, lang) {
    data = ""
    return new Promise((resolve) => {

        stateLanguage.states.forEach(element => {
            if (element[0] == name) {
                if (lang == "te") {
                    data = element
                }
                else if (lang == "en") {
                    data = element
                } else {
                    data = element
                }
            }

        })
        resolve(data)

    });
}

async function getStateName(statename) {
    return new Promise((resolve) => {
        var prev = 0;
        var data = "";
        states.states.forEach(element => {
            fuzz_ratio = fuzz.ratio(statename, element);
            if (fuzz_ratio >= 60 && prev <= fuzz_ratio) {
                prev = fuzz_ratio
                console.log(fuzz_ratio, element)
                data = element
                // data=requestPost(element)
            }
        });
        if (data == "")
            resolve({ "name": data, "status": false })
        else
            resolve({ "name": data, "status": true })
    });
}

var total;
async function requestPost(element) {
    console.log("element", element)
    return new Promise((resolve) => {
        try {
            var data;
            if (element == "India") {
                request('https://covid19-api.miraclesoft.com/states/data?stateName=Total', function (err, response, body) {
                    data = JSON.parse(body)
                    console.log("india count", data)
                    total = data
                    resolve(data)
                })
            }
            else {
                request('https://covid19-api.miraclesoft.com/states/data?stateName=' + element, function (err, response, body) {
                    data = JSON.parse(body)
                    console.log("active", data.data.active)

                    resolve(data)
                })
            }
        } catch (e) {
            console.log(e)
        }

    });
}


// ====================================  MAIN CLASS STARTS =========================================//
class DateState extends ComponentDialog {
    constructor(id) {
        super(id || 'DateState');
        this.addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new ChoicePrompt(CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.getState.bind(this),
                this.statePrompt.bind(this),
                this.option.bind(this),
                this.finalStep.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }    

    async getState(stepContext) {
        if (!stepContext.context.activity.state) {
            if (stepContext.context.activity.language == 'en')
                return await stepContext.prompt(TEXT_PROMPT, "Which state are you in?")
            else if (stepContext.context.activity.language == 'te')
                return await stepContext.prompt(TEXT_PROMPT, "మీరు ఏ రాష్ట్రంలో ఉన్నారో తెలియజేయండి?")
            else if (stepContext.context.activity.language == 'hi')
                return await stepContext.prompt(TEXT_PROMPT, "आप किस राज्य में हैं?")


        }
        else {
            return await stepContext.continueDialog(stepContext.context.activity.state);
        }
    }

    async statePrompt(stepContext) {
        //    var  stepContext.context.activity.language = stepContext.context.activity.language;
        //const statedetails = stepContext.options;
        var state = await getStateName(stepContext.result)
        console.log(state, "1111111111111111111111111111")
        if (!stepContext.context.activity.state) {
            stepContext.context.activity.state = state.name
            var sname = await getStateName(stepContext.result)
            var data = await requestPost(sname.name)
            total = await requestPost("India")
            console.log(data, "4444444444444444444444")
            if (data.status) {
                if (stepContext.context.activity.language == 'te') {
                    var a = await getStatelanguauge(sname.name, "te")
                    await stepContext.context.sendActivity(a[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                } else if (stepContext.context.activity.language == 'en') {
                    var a = await getStatelanguauge(sname.name, "en")
                    await stepContext.context.sendActivity(data.data.confirmed + " cases were reported in " + a[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + "  IST")
                } else if (stepContext.context.activity.language == 'hi') {
                    var a = await getStatelanguauge(sname.name, "hi")
                    await stepContext.context.sendActivity(data.data.confirmed + " मामले " + a[2] + " में दर्ज किए गए हैं और भारत में कुल 1251 मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                }
            } else {
                if (stepContext.context.activity.language == 'en') {
                    var a = await getStatelanguauge(sname.name, "en")
                    await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                } else if (stepContext.context.activity.language == 'te') {
                    var a = await getStatelanguauge(sname.name, "te")
                    await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                } else if (stepContext.context.activity.language == 'hi') {
                    var a = await getStatelanguauge(sname.name, "hi")
                    await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                }

            }
            return await stepContext.endDialog();
        }
        console.log("777777777777777777777777777777777777777777777777777", state)
        if (stepContext.context.activity.state) {
            if (stepContext.context.activity.language == "en") {
                var text = "For which location would you like to see the COVID-19 confirmed cases?";
                console.log("Inside state waterfall", stepContext)
                var reply = MessageFactory.suggestedActions(["India", stepContext.context.activity.state, 'Other'], text);
                await stepContext.context.sendActivity(reply);
                return await stepContext.prompt(TEXT_PROMPT, '');
            }

            if (stepContext.context.activity.language == "te") {
                var text = "మీరు ఏ ప్రదేశం లో COVID-19 గా ధృవీకరించబడిన కేసుల సంఖ్యను తెలుసుకోవాలనుకుంటున్నారు?";
                var a = await getStatelanguauge(stepContext.context.activity.state, "te")
                console.log("Inside state waterfall", stepContext)
                var reply = MessageFactory.suggestedActions(["భారతదేశం", a[1], 'ఇతర'], text);
                await stepContext.context.sendActivity(reply);
                return await stepContext.prompt(TEXT_PROMPT, '');
            }
            if (stepContext.context.activity.language == "hi") {
                var text = "किस स्थान के लिए आप COVID-19 पुष्ट मामलों को देखना चाहेंगे?";
                console.log("Inside state waterfall", stepContext)
                var a = await getStatelanguauge(stepContext.context.activity.state, "te")
                console.log("Inside state waterfall", stepContext)
                var reply = MessageFactory.suggestedActions(["भारत", a[2], 'अन्य'], text);
                await stepContext.context.sendActivity(reply);
                return await stepContext.prompt(TEXT_PROMPT, '');
            }
        } else {
            if (stepContext.context.activity.language == "en") {
                await stepContext.context.sendActivity("I’m sorry, that is not a state in India");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
            if (stepContext.context.activity.language == "te") {
                await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
            if (stepContext.context.activity.language == "hi") {
                await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला।");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
        }
    }

    async option(stepContext) {
        var options = stepContext.result

        if (stepContext.context.activity.language == "en") {
            if (options == 'Other') {
                return await stepContext.prompt(TEXT_PROMPT, "Which state would you like the information for?");
            }
            else if (options == 'India') {
                var data = await requestPost("India")
                if (data.status) {
                    if (stepContext.context.activity.language == 'te')
                        await stepContext.context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                    else if (stepContext.context.activity.language == 'en')
                        await stepContext.context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + "  IST")
                    else if (stepContext.context.activity.language == 'hi')
                        await stepContext.context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1]+" IST")
                } else {
                    if (stepContext.context.activity.language == "en") {
                        await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!");
                    }
                    if (stepContext.context.activity.language == "te") {
                        await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!");
                    }
                    if (stepContext.context.activity.language == "hi") {
                        await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!");
                    }
                }
                return await stepContext.endDialog();

            }
            else {
                console.log(total)
                var sname = await getStateName(options)
                var data = await requestPost(sname.name)
                console.log(data)
                total = await requestPost("India")
                console.log(total, "666666666666666666666666666666666")
                var a = await getStatelanguauge(sname.name, "te")
                if (sname.status) {
                    if (data.status) {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity(a[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                        } else if (stepContext.context.activity.language == 'en') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity(data.data.confirmed + " cases were reported in " + a[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity(data.data.confirmed + " मामले " + a[2] + " में दर्ज किए गए हैं और भारत में कुल 1251 मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        }
                    } else {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                        } else if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                        }
        
                    }
                } else {
                    if (stepContext.context.activity.language == 'te') {
                        await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'en') {
                        await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'hi') {
                        await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                }
                return await stepContext.endDialog();
            }
        }

        /*************************************************Telugu******************************************* */
        if (stepContext.context.activity.language == "te") {
            if (options == 'Other') {
                return await stepContext.prompt(TEXT_PROMPT, "మీరు ఏ రాష్ట్రంలో ఉన్నారో తెలియజేయండి?");

            }
            else if (options == 'India') {

                var data = await requestPost("India")
                if (data.status) {
                    if (stepContext.context.activity.language == 'te')
                        await stepContext.context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                    else if (stepContext.context.activity.language == 'en')
                        await stepContext.context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + "  IST")
                    else if (stepContext.context.activity.language == 'hi')
                        await stepContext.context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1]+" IST")
                } else {
                    if (stepContext.context.activity.language == 'en') {
                        var a = await getStatelanguauge(sname.name, "en")
                        await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                    } else if (stepContext.context.activity.language == 'te') {
                        var a = await getStatelanguauge(sname.name, "te")
                        await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                    } else if (stepContext.context.activity.language == 'hi') {
                        var a = await getStatelanguauge(sname.name, "hi")
                        await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                    }
                }
                return await stepContext.endDialog();
            }
            else {
                var sname = await getStateName(options)
                var data = await requestPost(sname.name)
                total = await requestPost("India")
                var a = await getStatelanguauge(sname.name, "te")
                if (sname.status) {
                    if (data.status) {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity(a[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                        } else if (stepContext.context.activity.language == 'en') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity(data.data.confirmed + " cases were reported in " + a[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity(data.data.confirmed + " मामले " + a[2] + " में दर्ज किए गए हैं और भारत में कुल 1251 मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        }
                    } else {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                        } else if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                        }
        
                    }
                } else {
                    if (stepContext.context.activity.language == 'te') {
                        await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'en') {
                        await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'hi') {
                        await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                }
                return await stepContext.endDialog();
            }
        }
        //*********************************************English****************************************** */
        if (stepContext.context.activity.language == "hi") {
            if (options == 'other') {
                return await stepContext.prompt(TEXT_PROMPT, "आप किस राज्य में हैं?");

            }
            else if (options == 'India') {

                var data = await requestPost("India")
                if (data.status) {
                    if (stepContext.context.activity.language == 'te')
                        await stepContext.context.sendActivity("భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం ధృవీకరించబడిన కేసులు సంఖ్య " + data.data.confirmed + " కి చేరుకుంది");
                    else if (stepContext.context.activity.language == 'en')
                        await stepContext.context.sendActivity("There are " + data.data.confirmed + " total cases reported in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + "  IST")
                    else if (stepContext.context.activity.language == 'hi')
                        await stepContext.context.sendActivity("भारत में " + data.data.confirmed + " कुल पुष्टि के मामले" + data.data.lastupdatedtime.split(" ")[0] + " दर्ज किए गए हैं " + data.data.lastupdatedtime.split(" ")[1])+" IST"
                } else {
                    if (stepContext.context.activity.language == "en") {
                        await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!");
                    }
                    if (stepContext.context.activity.language == "te") {
                        await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!");
                    }
                    if (stepContext.context.activity.language == "hi") {
                        await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!");
                    }
                }
                return await stepContext.endDialog();

            }
            else {
                var sname = await getStateName(options)
                var data = await requestPost(sname.name)
                total = await requestPost("India")
                if (sname.status) {
                    if (data.status) {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity(a[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                        } else if (stepContext.context.activity.language == 'en') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity(data.data.confirmed + " cases were reported in " + a[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity(data.data.confirmed + " मामले " + a[2] + " में दर्ज किए गए हैं और भारत में कुल 1251 मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                        }
                    } else {
                        if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                        } else if (stepContext.context.activity.language == 'te') {
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                        } else if (stepContext.context.activity.language == 'hi') {
                            var a = await getStatelanguauge(sname.name, "hi")
                            await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                        }
        
                    }
                } else {
                    if (stepContext.context.activity.language == 'te') {
                        await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'en') {
                        await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                    if (stepContext.context.activity.language == 'hi') {
                        await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                }
                return await stepContext.endDialog();
            }
        }
    }
    async finalStep(stepContext) {

        var sname = await getStateName(stepContext.result)
        var data = await requestPost(sname.name)
        total = await requestPost("India")
        if (sname.status) {
            if (data.status) {
                if (stepContext.context.activity.language == 'te') {
                    var a = await getStatelanguauge(sname.name, "te")
                    await stepContext.context.sendActivity(a[1] + " లో " + data.data.confirmed + " కేసులు నమోదయ్యాయి మరియు భారతదేశంలో " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + "  IST నాటికి మొత్తం కేసుల సంఖ్య " + total.data.confirmed + " కి చేరుకున్నాయి");
                } else if (stepContext.context.activity.language == 'en') {
                    var a = await getStatelanguauge(sname.name, "en")
                    await stepContext.context.sendActivity(data.data.confirmed + " cases were reported in " + a[0] + " and we have a total of " + total.data.confirmed + " cases in India as of " + data.data.lastupdatedtime.split(" ")[0] + " at " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                } else if (stepContext.context.activity.language == 'hi') {
                    var a = await getStatelanguauge(sname.name, "hi")
                    await stepContext.context.sendActivity(data.data.confirmed + " मामले " + a[2] + " में दर्ज किए गए हैं और भारत में कुल 1251 मामले हैं " + data.data.lastupdatedtime.split(" ")[0] + " " + data.data.lastupdatedtime.split(" ")[1] + " IST")
                }
            } else {
                if (stepContext.context.activity.language == 'te') {
                    var a = await getStatelanguauge(sname.name, "en")
                    await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!")
                } else if (stepContext.context.activity.language == 'te') {
                    var a = await getStatelanguauge(sname.name, "en")
                    await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!")
                } else if (stepContext.context.activity.language == 'hi') {
                    var a = await getStatelanguauge(sname.name, "hi")
                    await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!")
                }

            }
        } else {
            if (stepContext.context.activity.language == 'te') {
                await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు.");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
            if (stepContext.context.activity.language == 'en') {
                await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
            if (stepContext.context.activity.language == 'hi') {
                await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                console.log("Inside Test cebter valdjwjhdh", this.id)
                return await stepContext.beginDialog(this.id);
            }
        }
        return await stepContext.endDialog();


    }
}

module.exports.DateState = DateState;
