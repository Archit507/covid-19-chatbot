// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// ==========================================  IMPORT STATEMENTS =========================================//
const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
//var MiscResponse = require('../Responses/misc_response.json')
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog, ConfirmPrompt, ChoicePrompt } = require('botbuilder-dialogs');
const { Activity, ActivityTypes, Attachment, AttachmentLayoutTypes, CardAction, InputHints, SuggestedActions } = require('botframework-schema');
const { CardFactory, ActionTypes, MessageFactory } = require('botbuilder-core');
const { Language_name } = require('./mainDialog.js')
const TEXT_PROMPT = 'textPrompt';
const CONFIRM_PROMPT = 'confirmPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const CHOICE_PROMPT = 'choiceprompt'
var request = require('request')
var fuzz = require('fuzzball')
var states = require('../states.json');
var numbers = require('../Telephone.json')
var stateLanguage = require('../statelanguage.json')

//const REVIEW_SELECTION_DIALOG = 'REVIEW_SELECTION_DIALOG';

async function getStatelanguauge(name, lang) {
    data = ""
    return new Promise((resolve) => {

        stateLanguage.states.forEach(element => {
            if (element[0] == name) {
                if (lang == "te") {
                    data = element
                }
                else if (lang == "en") {
                    data = element
                } else {
                    data = element
                }
            }

        })
        resolve(data)

    });
}


async function getStateName(statename) {
    return new Promise((resolve) => {
        var prev = 0;
        var data = "";
        states.states.forEach(element => {
            fuzz_ratio = fuzz.ratio(statename, element);
            if (fuzz_ratio >= 60 && prev <= fuzz_ratio) {
                prev = fuzz_ratio
                console.log(fuzz_ratio, element)
                data = element
            }
        });
        if (data == "")
            resolve({ "name": data, "status": false })
        else
            resolve({ "name": data, "status": true })
    });
}

async function getTestCenter(element, language) {
    console.log("element", element)
    return new Promise((resolve) => {
        var data;
        request('https://covid19-api.miraclesoft.com/testCenter/retrieve?stateName=' + element + '&language=' + language,
            function (err, response, body) {
                if (body) {
                    data = JSON.parse(body)
                    console.log(data)
                    resolve(data)
                } else {
                    resolve({ data: false })
                }
            })

    });
}



// ====================================  MAIN CLASS STARTS =========================================//
class TestCenter extends ComponentDialog {
    constructor(id) {
        super(id || 'TestCenter');
        this.addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
            .addDialog(new ChoicePrompt(CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.getState.bind(this),
                this.statePrompt.bind(this),
                this.option.bind(this),
                this.Statename.bind(this)
            ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }
    async getState(stepContext) {
        if (!stepContext.context.activity.state) {
            if (stepContext.context.activity.language == 'en')
                return await stepContext.prompt(TEXT_PROMPT, "Which state are you in?")
            else if (stepContext.context.activity.language == 'te')
                return await stepContext.prompt(TEXT_PROMPT, "మీరు ఏ రాష్ట్రంలో ఉన్నారో తెలియజేయండి?")
            else if (stepContext.context.activity.language == 'hi')
                return await stepContext.prompt(TEXT_PROMPT, "आप किस राज्य में हैं?")
        }
        else {
            return await stepContext.continueDialog(stepContext.context.activity.state);
        }
    }
    async statePrompt(stepContext) {
        //const statedetails = stepContext.options;
        // if (!stepContext.context.activity.state) {
        //     await stepContext.context.sendActivity("No state")
        //     return await stepContext.endDialog();
        // }
        var state = await getStateName(stepContext.result)
        if (!stepContext.context.activity.state) {
            stepContext.context.activity.state = state.name
            try {
                console.log("777777777777777777777777")
                if (stepContext.context.activity.language == "en") {
                    var sname = await getStateName(stepContext.result)
                    var testcenters = await getTestCenter(sname.name, "English")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)
                            })
                            var a = await getStatelanguauge(sname.name, "en")
                            console.log(a)
                            await stepContext.context.sendActivity("We have the following testing centers in " + a[0] + ",");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        }
                        else {
                            await stepContext.context.sendActivity("Unfortunately there are no testing centers for COVID-19 currently available in " + stepContext.result);
                            return await stepContext.endDialog();
                        }
                    }
                    else {
                        await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                }
    
                //*********************************Telugu********************************************* */
                if (stepContext.context.activity.language == "te") {
                    var sname = await getStateName(stepContext.result)
                    var testcenters = await getTestCenter(sname.name, "Telugu")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)
    
                            })
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity(a[1] + " లో ఈ క్రింది పరీక్షా కేంద్రాలు ఉన్నాయి,");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        } else {//not available
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity("దురదృష్టవశాత్తు ప్రస్తుతం " + a[1] + " లో COVID-19 కోసం ఏ పరీక్షా కేంద్రాలు అందుబాటులో లేవు");
                            // return await stepContext.beginDialog(this.id);
                            return await stepContext.endDialog();
                        }
                    } else {//no state
                        await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు. భారతదేశంలో మీరు ఏ రాష్ట్రానికి చెందిన సమాచారం తెలుసు కోవాలనుకుంటున్నారు?");
                        return await stepContext.beginDialog(this.id);
                    }
                   
                }
                //*********************************************Hindi**************************************** */
                if (stepContext.context.activity.language == "hi") {
                    var sname = await getStateName(stepContext.result)
                    var testcenters = await getTestCenter(sname.name, "Hindi")
                    var a = await getStatelanguauge(sname.name, "te")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)
    
                            })
                            await stepContext.context.sendActivity("हमारे पास " + a[2] + " में निम्नलिखित परीक्षण केंद्र हैं,");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        } else {//no data
                            await stepContext.context.sendActivity("दुर्भाग्य से " + a[2] + "में वर्तमान में COVID-19 के लिए कोई परीक्षण केंद्र नहीं हैं ");
                            return await stepContext.endDialog();
                        }
                    } else {//no state
                        await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                        return await stepContext.beginDialog(this.id);
                    }
                }
            } catch (e) {
                if (stepContext.context.activity.language == "en") {
                    await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!");
                }
                if (stepContext.context.activity.language == "te") {
                    await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!");
                }
                if (stepContext.context.activity.language == "hi") {
                    await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!");
                }
                return await stepContext.endDialog();
            }
        
        }
        if (stepContext.context.activity.language == "en") {
            console.log("Inside state waterfall", stepContext)
            //var lang_name = stepContext.context.activity.text;
            //console.log(lang_name)
            //await stepContext.context.sendActivity("Yes, there are testing centers available across the country");
            var reply = MessageFactory.suggestedActions([stepContext.context.activity.state, 'Other'], 'There are testing centers available across the country. Which location would you like the information for?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
        }
        //*****************************Telugu********************************* */

        else if (stepContext.context.activity.language == "te") {
            //var a = "అవును, దేశవ్యాప్తంగా పరీక్షా కేంద్రాలు అందుబాటులో ఉన్నాయి. మీరు ఏ రాష్ట్రములో  పరీక్షా కేంద్రాలను తెలుసుకోవాలనుకుంటున్నారు?";
            console.log("Inside state waterfall", stepContext)
            var a = await getStatelanguauge(stepContext.context.activity.state, "te")
            var reply = MessageFactory.suggestedActions([a[1], 'ఇతర'], 'దేశవ్యాప్తంగా పరీక్షా కేంద్రాలు అందుబాటులో ఉన్నాయి. మీరు ఏ రాష్ట్రములో  పరీక్షా కేంద్రాలను తెలుసుకోవాలనుకుంటున్నారు?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
        }

        //*******************************Hindi************************************ */
        else if (stepContext.context.activity.language == "hi") {
            console.log("Inside state waterfall", stepContext)
            var a = await getStatelanguauge(stepContext.context.activity.state, "hi")
            var reply = MessageFactory.suggestedActions([a[2], 'अन्य'], 'देश भर में परीक्षण केंद्र उपलब्ध हैं। आप किस राज्य के लिए परीक्षण केंद्र देखना चाहेंगे?');
            await stepContext.context.sendActivity(reply);
            return await stepContext.prompt(TEXT_PROMPT, '');
        }
    }



    async option(stepContext) {
        var options = stepContext.result
        //==========================English======================//
        try {
            if (stepContext.context.activity.language == "en") {
                if (stepContext.context.activity.text == 'Other') {
                    return await stepContext.prompt(TEXT_PROMPT, "Which state would you like the information for?");

                }
                else if (stepContext.context.activity.text == stepContext.context.activity.state) {
                    var sname = await getStateName(stepContext.result)
                    var testcenters = await getTestCenter(sname.name, "English")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)

                            })
                            await stepContext.context.sendActivity("We have the following testing centers in " + sname.name + ",");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        } else {//no data
                            var a = await getStatelanguauge(sname.name, "en")
                            await stepContext.context.sendActivity("Unfortunately there are no testing centers for COVID-19 currently available in " + a[0]);
                            // return await stepContext.beginDialog(this.id);
                            return await stepContext.endDialog();
                        }
                    } else {//no state
                        await stepContext.context.sendActivity("I’m sorry, but we were not able to find that state. I can only provide data for Indian states.");
                        console.log("Inside Test cebter valdjwjhdh", this.id)
                        return await stepContext.beginDialog(this.id);
                    }
                }
                else {
                    await stepContext.context.sendActivity("Please try again");
                    return await stepContext.endDialog();
                }
            }

            //*************************************************Telugu******************************************* */
            if (stepContext.context.activity.language == "te") {
                // console.log(options, "*****************************************************************************")
                if (stepContext.context.activity.text == 'Other') {
                    return await stepContext.prompt(TEXT_PROMPT, "మీరు ఏ రాష్ట్రానికి సంబంధించిన సమాచారం తెలుసుకోవాలనుకుంటున్నాను?");

                }
                else if (stepContext.context.activity.text == stepContext.context.activity.state) {

                    var sname = await getStateName(stepContext.result)
                    var testcenters = await getTestCenter(sname.name, "Telugu")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)

                            })
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity(a[1] + " లో ఈ క్రింది పరీక్షా కేంద్రాలు ఉన్నాయి,");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        } else {//not available
                            var a = await getStatelanguauge(sname.name, "te")
                            await stepContext.context.sendActivity("దురదృష్టవశాత్తు ప్రస్తుతం " + a[1] + " లో COVID-19 కోసం ఏ పరీక్షా కేంద్రాలు అందుబాటులో లేవు");
                            // return await stepContext.beginDialog(this.id);
                            return await stepContext.endDialog();
                        }
                    } else {//no state
                        await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు. భారతదేశంలో మీరు ఏ రాష్ట్రానికి చెందిన సమాచారం తెలుసు కోవాలనుకుంటున్నారు?");
                        return await stepContext.beginDialog(this.id);
                    }
                }
                else {
                    await stepContext.context.sendActivity("Please try again");
                    return await stepContext.endDialog();
                }
            }
            //*********************************************Hindi****************************************** */
            if (stepContext.context.activity.language == "hi") {
                if (stepContext.context.activity.text == 'other') {
                    return await stepContext.prompt(TEXT_PROMPT, "आप किस राज्य में हैं?");

                }
                else if (stepContext.context.activity.text == stepContext.context.activity.state) {
                    var sname = await getStateName(options)
                    var testcenters = await getTestCenter(sname.name, "Hindi")
                    var a = await getStatelanguauge(sname.name, "te")
                    if (sname.status) {
                        if (testcenters.status &&testcenters.data[0].center.length!=0) {
                            console.log(testcenters.data[0].center)
                            var cards = []
                            testcenters.data[0].center.forEach(test => {
                                var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                                cards.push(card)

                            })
                            await stepContext.context.sendActivity("हमारे पास " + a[2] + " में निम्नलिखित परीक्षण केंद्र हैं,");
                            await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                            return await stepContext.endDialog();
                        } else {//no data
                            await stepContext.context.sendActivity("दुर्भाग्य से " + a[2] + "में वर्तमान में COVID-19 के लिए कोई परीक्षण केंद्र नहीं हैं ");
                            return await stepContext.endDialog();
                        }
                    } else {//no state
                        await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                        // return await stepContext.beginDialog(this.id);
                        return await stepContext.endDialog();
                    }
                }
                else {
                    await stepContext.context.sendActivity("Please try again");
                    return await stepContext.endDialog();
                }
            }

        } catch (e) {
            console.log(e)
            if (stepContext.context.activity.language == "en") {
                await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!");
            }
            if (stepContext.context.activity.language == "te") {
                await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!");
            }
            if (stepContext.context.activity.language == "hi") {
                await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!");
            }
            return await stepContext.endDialog();
        }

    }

    ///////////////////////////other/////////////////////////////////////////

    async Statename(stepContext) {
        try {
            console.log("777777777777777777777777")
            if (stepContext.context.activity.language == "en") {
                var sname = await getStateName(stepContext.result)
                var testcenters = await getTestCenter(sname.name, "English")
                if (sname.status) {
                    if (testcenters.status &&testcenters.data[0].center.length!=0) {
                        console.log(testcenters.data[0].center)
                        var cards = []
                        testcenters.data[0].center.forEach(test => {
                            var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                            cards.push(card)
                        })
                        var a = await getStatelanguauge(sname.name, "en")
                        console.log(a)
                        await stepContext.context.sendActivity("We have the following testing centers in " + a[0] + ",");
                        await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                        return await stepContext.endDialog();
                    }
                    else {
                        await stepContext.context.sendActivity("Unfortunately there are no testing centers for COVID-19 currently available in" + stepContext.result);
                        return await stepContext.endDialog();
                    }
                }
                else {
                    await stepContext.context.sendActivity("I’m sorry, that is not a state in India - please specify the correct state.");
                    console.log("Inside Test cebter valdjwjhdh", this.id)
                    return await stepContext.beginDialog(this.id);
                }
            }

            //*********************************Telugu********************************************* */
            if (stepContext.context.activity.language == "te") {
                var sname = await getStateName(stepContext.result)
                var testcenters = await getTestCenter(sname.name, "Telugu")
                if (sname.status) {
                    if (testcenters.status &&testcenters.data[0].center.length!=0) {
                        console.log(testcenters.data[0].center)
                        var cards = []
                        testcenters.data[0].center.forEach(test => {
                            var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                            cards.push(card)

                        })
                        var a = await getStatelanguauge(sname.name, "te")
                        await stepContext.context.sendActivity(a[1] + " లో ఈ క్రింది పరీక్షా కేంద్రాలు ఉన్నాయి,");
                        await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                        return await stepContext.endDialog();
                    } else {//not available
                        var a = await getStatelanguauge(sname.name, "te")
                        await stepContext.context.sendActivity("దురదృష్టవశాత్తు ప్రస్తుతం " + a[1] + " లో COVID-19 కోసం ఏ పరీక్షా కేంద్రాలు అందుబాటులో లేవు");
                        // return await stepContext.beginDialog(this.id);
                        return await stepContext.endDialog();
                    }
                } else {//no state
                    await stepContext.context.sendActivity("క్షమించండి, ఆ ప్రదేశం యొక్క సమాచారం నా దగ్గర లేదు. భారతదేశంలో మీరు ఏ రాష్ట్రానికి చెందిన సమాచారం తెలుసు కోవాలనుకుంటున్నారు?");
                    return await stepContext.beginDialog(this.id);
                }

            }
            //*********************************************Hindi**************************************** */
            if (stepContext.context.activity.language == "hi") {
                var sname = await getStateName(stepContext.result)
                var testcenters = await getTestCenter(sname.name, "Hindi")
                var a = await getStatelanguauge(sname.name, "te")
                if (sname.status) {
                    if (testcenters.status &&testcenters.data[0].center.length!=0) {
                        console.log(testcenters.data[0].center)
                        var cards = []
                        testcenters.data[0].center.forEach(test => {
                            var card = CardFactory.heroCard(test.CenterName, test.City, [], CardFactory.actions([{ type: ActionTypes.OpenUrl, title: "Map Link", value: test.mapLink }]))
                            cards.push(card)

                        })
                        await stepContext.context.sendActivity("हमारे पास " + a[2] + " में निम्नलिखित परीक्षण केंद्र हैं,");
                        await stepContext.context.sendActivity({ attachments: cards, attachmentLayout: AttachmentLayoutTypes.Carousel });
                        return await stepContext.endDialog();
                    } else {//no data
                        await stepContext.context.sendActivity("दुर्भाग्य से " + a[2] + "में वर्तमान में COVID-19 के लिए कोई परीक्षण केंद्र नहीं हैं ");
                        return await stepContext.endDialog();
                    }
                } else {//no state
                    await stepContext.context.sendActivity("मुझे खेद है, लेकिन हमें वह राज्य नहीं मिला। मैं केवल भारतीय राज्यों के डेटा प्रदान कर सकता हूं।");
                    return await stepContext.beginDialog(this.id);
                }
            }
        } catch (e) {
            if (stepContext.context.activity.language == "en") {
                await stepContext.context.sendActivity("Sorry, we are unable to get the data currently. Please try again later!");
            }
            if (stepContext.context.activity.language == "te") {
                await stepContext.context.sendActivity("క్షమించండి, మేము మీరు అడిగిన  సమాచారాన్ని చూపించలేక పోతున్నాము. దయచేసి కాసేపు తర్వాత మళ్లీ ప్రయత్నించండి!");
            }
            if (stepContext.context.activity.language == "hi") {
                await stepContext.context.sendActivity("क्षमा करें, हम वर्तमान में डेटा प्राप्त करने में असमर्थ हैं। बाद में पुन: प्रयास करें!");
            }
            return await stepContext.endDialog();
        }
    }
}

module.exports.TestCenter = TestCenter;
